<!--
  #%L
  JAXX :: Widgets Status
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->


<Table border='{BorderFactory.createBevelBorder(BevelBorder.LOWERED)}' insets='0'
       implements='java.awt.event.ActionListener'>

  <import>
    java.awt.Component
    java.awt.event.ActionEvent
    javax.swing.BorderFactory
    javax.swing.border.BevelBorder
  </import>

  <script><![CDATA[

// To ensure status bar constant height, no matter what font are in use...
protected final static String EMPTY_STATUS = " ";

public void clearStatus() {
    handler.clearStatus();
}

public void startProgress() {
    startProgress(null);
}

public void startProgress(String status) {
    setBusy(true);
    setStatus(status);
}

public void stopProgress() {
  stopProgress(null);
}

public void stopProgress(String finalStatus) {
    setBusy(false);
    setStatus(finalStatus);
}

public void setStatus(String status) {
    handler.setStatus(status);
}

@Override
public void actionPerformed(ActionEvent evt) {
    handler.fadeStatus(this);
}

public <U extends Component> U getWidget(Class<U> clazz) {
    for (Component component : box.getComponents()) {
        if (clazz == component.getClass()) {
            return (U) component;
        }
    }
    return null;
}

public void addWidget(Component w) {
    box.add(w);
}

public void addWidget(Component w, int index) {
    box.add(w, index);
}

public void init() {
    handler.init();
}
]]>
  </script>

  <Boolean id='showMemoryStatus' javaBean='Boolean.TRUE'/>
  <Boolean id='showClock' javaBean='Boolean.TRUE'/>
  <Boolean id='showBusy' javaBean='Boolean.FALSE'/>
  <Boolean id='busy' javaBean='Boolean.FALSE'/>

  <row>
    <cell anchor='west' fill='both' weightx='1'>
      <Box constructorParams='0'>
        <JProgressBar id='busyWidget' visible='{isShowBusy()}' enabled='{isBusy()}' indeterminate='{isBusy()}'
                      stringPainted='false' borderPainted='true'/>
        <JLabel id='statusLabel'/>

      </Box>

    </cell>
    <cell anchor='east'>
      <Box id='box' constructorParams='0'>
        <MemoryStatusWidget visible="{ isShowMemoryStatus() }"/>
        <ClockWidget visible="{ isShowClock() }"/>
      </Box>
    </cell>
  </row>
</Table>
