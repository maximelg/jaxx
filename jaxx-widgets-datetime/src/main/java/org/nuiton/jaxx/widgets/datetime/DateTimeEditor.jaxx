<!--
  #%L
  JAXX :: Widgets DateTime
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<JPanel layout='{new BorderLayout()}'>

  <import>
    java.util.Calendar
    java.util.Date
    java.io.Serializable
    java.awt.BorderLayout

    org.nuiton.jaxx.widgets.hidor.HidorButton
    javax.swing.DefaultBoundedRangeModel

    org.jdesktop.swingx.JXDatePicker
  </import>

  <!-- model -->
  <DateTimeEditorModel id='model'/>

  <!-- spinner hour editor -->
  <SpinnerDateModel id="hourModel"/>

  <!-- spinner minute editor -->
  <SpinnerDateModel id="minuteModel"/>

  <!-- flag to show or hide the time slider (this state can be saved by SwingSession) -->
  <Boolean id='showTimeEditorSlider' javaBean='true'/>

  <script><![CDATA[
public void init() { handler.init(this); }

public void setPropertyTimeDate(String property) { model.setPropertyTimeDate(property); }
public void setPropertyDayDate(String property) { model.setPropertyDayDate(property); }
public void setPropertyDate(String property) { model.setPropertyDate(property); }
public void setDateEditable(boolean dateEditable) { model.setDateEditable(dateEditable); }
public void setTimeEditable(boolean timeEditable) { model.setTimeEditable(timeEditable); }
public void setBean(Serializable bean) { model.setBean(bean); }
public void setDate(Date date) { model.setDate(date); }

public void setDateFormat(String dateFormat) { dayDateEditor.setFormats(dateFormat); }
public void setLabel(String label) { dateTimeLabel.setText(label); }
]]>
  </script>

  <BeanValidator id='validator' autoField='true' bean='model'>
    <field name='date' component='dateEditor'/>
    <field name='dayDate' component='dayDateEditor'/>
    <field name='timeDate' component='timeDateEditor'/>
  </BeanValidator>

  <JPanel id='dateEditor' constraints='BorderLayout.NORTH' layout='{new BorderLayout()}'>
    <JLabel id='dateTimeLabel' constraints='BorderLayout.WEST'/>
    <JPanel constraints='BorderLayout.EAST'>
      <JXDatePicker id='dayDateEditor'
                    onActionPerformed='model.setDayDate((Date)((JXDatePicker)event.getSource()).getDate())'/>
      <JPanel id='timeDateEditor'>
        <JSpinner id='hourEditor' onStateChanged='handler.setHours((Date)((JSpinner)event.getSource()).getValue())'/>
        <JLabel id='labelH'/>
        <JSpinner id='minuteEditor'
                  onStateChanged='handler.setMinutes((Date)((JSpinner)event.getSource()).getValue())'/>
      </JPanel>
      <JToolBar id="sliderHidorToolBar">
        <HidorButton id='sliderHidor'/>
      </JToolBar>
    </JPanel>
  </JPanel>

  <JSlider id='slider' constraints='BorderLayout.SOUTH'
           onStateChanged='if (!slider.getValueIsAdjusting()) model.setTimeInMinutes((Integer)((JSlider)event.getSource()).getValue());'/>

</JPanel>
