package org.nuiton.jaxx.runtime.swing.list.filter;

/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


public enum CheckListFilterType {

    STARTS_WITH {
        @Override
        public boolean include(String element, String filter) {

            if (element == null || filter == null) return false;
            return element.startsWith(filter);

        }

    },

    CONTAINS {
        @Override
        public boolean include(String element, String filter) {

            if (element == null || filter == null) return false;
            return element.toLowerCase().contains(filter.toLowerCase());

        }

    };

    public abstract boolean include(String element, String filter);

}
