/*
 * Copyright (c) 2009-2011, EzWare
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.Redistributions
 * in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.Neither the name of the
 * EzWare nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * %%Ignore-License%%
 */

package org.nuiton.jaxx.runtime.swing.table.filter;

import org.nuiton.decorator.Decorator;

import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.util.Collections;

public final class TableRowFilterSupport {

    private boolean searchable = false;
    private Decorator<Object> decorator;
    private final TableFilter<?> filter;
    private boolean actionsVisible = true;
    private boolean useTableRenderers = false;
    private Dimension popupDefaultSize = null;

    private TableRowFilterSupport(TableFilter<?> filter) {
        if (filter == null) throw new NullPointerException();
        //this.table = table;
        this.filter = filter;
    }

    public static TableRowFilterSupport forFilter(TableFilter<?> filter) {
        return new TableRowFilterSupport(filter);
    }

    /**
     * Additional actions visible in column filter list
     *
     * @param visible
     * @return
     */
    public TableRowFilterSupport actions(boolean visible) {
        this.actionsVisible = visible;
        return this;
    }

    /**
     * Comlumn filter list is searchable
     *
     * @param searchable
     * @return
     */
    public TableRowFilterSupport searchable(boolean searchable) {
        this.searchable = searchable;
        return this;
    }

    public TableRowFilterSupport searchDecorator(Decorator<Object> decorator) {
        this.decorator = decorator;
        return this;
    }

    public TableRowFilterSupport useTableRenderers(boolean value) {
        this.useTableRenderers = value;
        return this;
    }

    public TableRowFilterSupport setPopupDefaultSize(Dimension popupDefaultSize) {
        this.popupDefaultSize = popupDefaultSize;
        return this;
    }

    public JTable apply() {

        final TableFilterColumnPopup filterPopup = new TableFilterColumnPopup(filter);
        filterPopup.setEnabled(true);
        filterPopup.setActionsVisible(actionsVisible);
        filterPopup.setSearchable(searchable);
        filterPopup.searchDecorator(decorator);
        filterPopup.setUseTableRenderers(useTableRenderers);
        filterPopup.setDefaultSize(popupDefaultSize);

        setupTableHeader();

        return filter.getTable();
    }

    private void setupTableHeader() {

        final JTable table = filter.getTable();

        filter.addChangeListener(filter -> table.getTableHeader().repaint());

        // make sure that search component is reset after table model changes
        setupHeaderRenderers(table.getModel(), true);
//		table.addPropertyChangeListener("model", new PropertyChangeListener() {
//
//			public void propertyChange(PropertyChangeEvent evt) {
//
//				FilterTableHeaderRenderer headerRenderer = new FilterTableHeaderRenderer(filter);
//				filter.modelChanged((TableModel) evt.getNewValue());
//
//				for( TableColumn c:  Collections.list( filter.getTable().getColumnModel().getColumns()) ) {
//					c.setHeaderRenderer( headerRenderer );
//				}
//			}}
//
//		);
    }

    private void setupHeaderRenderers(TableModel newModel, boolean fullSetup) {

        final JTable table = filter.getTable();

        FilterTableHeaderRenderer headerRenderer = new FilterTableHeaderRenderer(filter);
        filter.modelChanged(newModel);

        for (TableColumn c : Collections.list(table.getColumnModel().getColumns())) {
            c.setHeaderRenderer(headerRenderer);
        }

        if (!fullSetup) return;

        PropertyChangeListener listener = e -> setupHeaderRenderers(table.getModel(), false);
        table.addPropertyChangeListener("model", listener);
        table.addPropertyChangeListener("columnModel", listener);

    }

}
