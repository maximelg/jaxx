/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.editor;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import org.syntax.jedit.JEditTextArea;
import org.syntax.jedit.tokenmarker.JavaTokenMarker;
import org.syntax.jedit.tokenmarker.TokenMarker;
import org.syntax.jedit.tokenmarker.XMLTokenMarker;

import javax.swing.JPanel;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * RSyntaxTextArea editor implementation.
 *
 * @author chatellier
 * @version $Revision$
 *
 *          Last update : $Date$
 *          By : $Author$
 */
public class JEditEditor extends JPanel implements EditorInterface, DocumentListener {

    /** serialVersionUID. */
    private static final long serialVersionUID = 5880160718377536089L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    private static final Log log = LogFactory.getLog(JEditEditor.class);

    protected final JEditTextArea editor;

    protected boolean isModified = false;

    public JEditEditor() {
        editor = new JEditTextArea();

        setLayout(new BorderLayout());
        add(editor, BorderLayout.CENTER);
    }

    @Override
    public boolean accept(File file) {
        String ext = FileUtil.extension(file);
        boolean result = "java".equalsIgnoreCase(ext);
        result = result || "xml".equalsIgnoreCase(ext);
        return result;
    }

    @Override
    public boolean accept(Editor.EditorSyntaxConstant editorSyntaxConstant) {
        return editorSyntaxConstant.isSupported(Editor.EditorSyntaxConstant.JAVA, Editor.EditorSyntaxConstant.XML);
    }

    @Override
    public void setSyntax(Editor.EditorSyntaxConstant editorSyntax) {
        TokenMarker tokenMarker = null;
        if (editorSyntax.equals(Editor.EditorSyntaxConstant.JAVA)) {
            tokenMarker = new JavaTokenMarker();
        } else if (editorSyntax.equals(Editor.EditorSyntaxConstant.XML)) {
            tokenMarker = new XMLTokenMarker();
        } else {
            log.warn("Syntax '" + editorSyntax.getName() + "' is not yet supported by JEditEditor");
        }

        if (tokenMarker != null) {
            editor.setTokenMarker(tokenMarker);
        }
    }

    @Override
    public boolean open(File file) {

        try {
            editor.getDocument().removeDocumentListener(this);

            Reader in = new BufferedReader(new FileReader(file));
            // editor.read(in, file);
            // String text = editor.getText();

            String text = "";
            char c;
            int last;

            while ((last = in.read()) != -1) {
                c = (char) last;
                // on peut avoir \r\n (windows) \r (macos) \n (unix)
                if ('\r' == c) { // pour windows et macos on remplace par \n
                    in.mark(1);
                    last = in.read();
                    if (last != -1) {
                        if ('\n' != (char) last) {
                            // on a seulement \r on remet le dernier caractere
                            // lu
                            in.reset();
                        }
                        // dans tous les cas \r ou \r\n on remplace par \n
                        c = '\n';
                    }
                }
                text += c;
            }

            String ext = FileUtil.extension(file);
            if ("java".equalsIgnoreCase(ext)) {
                editor.setTokenMarker(new JavaTokenMarker());
            } else if ("xml".equalsIgnoreCase(ext)) {
                editor.setTokenMarker(new XMLTokenMarker());
            }

            editor.setText(text);
            editor.setCaretPosition(0);
            editor.getDocument().addDocumentListener(this);
            isModified = false;
            return true;
        } catch (FileNotFoundException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't find file: " + file, eee);
            }
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't open file: " + file, eee);
            }
        }
        return false;
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        editor.getDocument().addDocumentListener(listener);
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        editor.getDocument().removeDocumentListener(listener);
    }

    @Override
    public void addCaretListener(CaretListener listener) {
        editor.addCaretListener(listener);
    }

    @Override
    public void removeCaretListener(CaretListener listener) {
        editor.removeCaretListener(listener);
    }

    @Override
    public String getText() {
        return editor.getText();
    }

    @Override
    public void setEnabled(boolean b) {
        super.setEnabled(b);
        editor.setEnabled(b);
    }

    @Override
    public boolean isModified() {
        return isModified;
    }

    @Override
    public boolean saveAs(File file) {
        try {
            FileUtils.write(file, editor.getText(), "utf-8");
            isModified = false;
            return true;
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't save file", eee);
            }
        }
        return false;
    }

    @Override
    public void setText(String text) {
        editor.setText(text);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void copy() {
        editor.copy();
    }

    @Override
    public void cut() {
        editor.cut();
    }

    @Override
    public void paste() {
        editor.paste();
    }
}
