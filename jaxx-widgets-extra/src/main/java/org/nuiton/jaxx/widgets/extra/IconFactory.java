/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * IconFactory.java
 *
 * Created: Jul 29, 2004
 *
 * @author Cédric Pineau <pineau@codelutin.com>
 *
 * @version $Revision$
 *
 * Last update : $Date$ by : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import org.apache.commons.collections4.map.AbstractReferenceMap;
import org.apache.commons.collections4.map.ReferenceIdentityMap;
import org.nuiton.util.Resource;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.util.IdentityHashMap;
import java.util.Map;

public class IconFactory {

    protected static IconFactory _instance = null;

    protected static IconFactory getInstance() {
        if (_instance == null) {
            _instance = new IconFactory();
        }
        return _instance;
    }

    public static final String APPEARANCE = "APPEARANCE";
    public static final String APPOINTMENT = "APPOINTMENT";
    public static final String ARROW1DOWN = "DOWN";
    public static final String ARROW1LEFT = "LEFT";
    public static final String ARROW1RIGHT = "RIGHT";
    public static final String ARROW1UP = "UP";
    public static final String ARROW2DOWN = "DOWN";
    public static final String ARROW2LEFT = "LEFT";
    public static final String ARROW2RIGHT = "RIGHT";
    public static final String ARROW2UP = "UP";
    public static final String ATTACH = "ATTACH";
    public static final String CANCEL = "CANCEL";
    public static final String CLOSE = "CLOSE";
    public static final String FIND = "FIND";
    public static final String FONTS = "FONTS";
    public static final String FULLSCREEN = "FULLSCREEN";
    public static final String HELP = "HELP";
    public static final String IDEA = "IDEA";
    public static final String INFO = "INFO";
    public static final String LINK = "LINK";
    public static final String MAIL = "MAIL";
    public static final String MAILGET = "MAILGET";
    public static final String MAILNEW = "MAILNEW";
    public static final String MAILSEND = "MAILSEND";
    public static final String NEW = "NEW";
    public static final String NOFULLSCREEN = "NOFULLSCREEN";
    public static final String OK = "OK";
    public static final String OPEN = "OPEN";
    public static final String PRINT = "PRINT";
    public static final String QUIT = "QUIT";
    public static final String REDO = "REDO";
    public static final String RESET = "RESET";
    public static final String RUN = "RUN";
    public static final String SAVE = "SAVE";
    public static final String SAVEAS = "SAVEAS";
    public static final String SPELLCHECK = "SPELLCHECK";
    public static final String STOP = "STOP";
    public static final String TIME = "TIME";
    public static final String UNDO = "UNDO";

    protected final Map<Object, String> iconURLs;

    protected IconFactory() {
        iconURLs = new IdentityHashMap<>();

        iconURLs.put(APPEARANCE, "/22x22/appearance.png");
        iconURLs.put(APPOINTMENT, "/22x22/appointment.png");
        iconURLs.put(ARROW1DOWN, "/22x22/arrow1Down.png");
        iconURLs.put(ARROW1LEFT, "/22x22/arrow1Left.png");
        iconURLs.put(ARROW1RIGHT, "/22x22/arrow1Right.png");
        iconURLs.put(ARROW1UP, "/22x22/arrow1Up.png");
        iconURLs.put(ARROW2DOWN, "/22x22/arrow2Down.png");
        iconURLs.put(ARROW2LEFT, "/22x22/arrow2Left.png");
        iconURLs.put(ARROW2RIGHT, "/22x22/arrow2Right.png");
        iconURLs.put(ARROW2UP, "/22x22/arrow2Up.png");
        iconURLs.put(ATTACH, "/22x22/attach.png");
        iconURLs.put(CANCEL, "/22x22/cancel.png");
        iconURLs.put(CLOSE, "/22x22/close.png");
        iconURLs.put(FIND, "/22x22/find.png");
        iconURLs.put(FONTS, "/22x22/fonts.png");
        iconURLs.put(FULLSCREEN, "/22x22/fullscreen.png");
        iconURLs.put(HELP, "/22x22/help.png");
        iconURLs.put(IDEA, "/22x22/idea.png");
        iconURLs.put(INFO, "/22x22/info.png");
        iconURLs.put(LINK, "/22x22/redo.png");
        iconURLs.put(MAIL, "/22x22/mail.png");
        iconURLs.put(MAILGET, "/22x22/mailGet.png");
        iconURLs.put(MAILNEW, "/22x22/mailNew.png");
        iconURLs.put(MAILSEND, "/22x22/mailSend.png");
        iconURLs.put(NEW, "/22x22/new.png");
        iconURLs.put(NOFULLSCREEN, "/22x22/nofullscreen.png");
        iconURLs.put(OK, "/22x22/ok.png");
        iconURLs.put(OPEN, "/22x22/open.png");
        iconURLs.put(PRINT, "/22x22/print.png");
        iconURLs.put(QUIT, "/22x22/close.png");
        iconURLs.put(REDO, "/22x22/redo.png");
        iconURLs.put(RESET, "/22x22/reset.png");
        iconURLs.put(RUN, "/22x22/run.png");
        iconURLs.put(SAVE, "/22x22/save.png");
        iconURLs.put(SAVEAS, "/22x22/saveAs.png");
        iconURLs.put(SPELLCHECK, "/22x22/spellcheck.png");
        iconURLs.put(STOP, "/22x22/stop.png");
        iconURLs.put(TIME, "/22x22/time.png");
        iconURLs.put(UNDO, "/22x22/undo.png");
    }

    static protected final ReferenceIdentityMap<Object, Icon> cache = new ReferenceIdentityMap<>(
            AbstractReferenceMap.ReferenceStrength.HARD, AbstractReferenceMap.ReferenceStrength.SOFT);

    /**
     * Retourne l'icone demandé.
     *
     * @param iconId l'identifiant de l'icon, il doit exister, par exemple
     *               IconFactory.getIcon(IconFactory.UNDO) et non pas
     *               IconFactory.getIcon("UNDO")
     * @return null si l'icon n'a pas public etre retrouve alors qu'il devrait
     * exister.
     * @throws IllegalArgumentException si l'idendifiant de l'icon n'existe pas
     */
    static public Icon getIcon(Object iconId) {
        Icon result = null;
        if (iconId != null) {
            if (getInstance().iconURLs.containsKey(iconId)) {
                result = cache.get(iconId);
                if (result == null) {
                    result = Resource.getIcon(getInstance().iconURLs.get(iconId));
                    if (result != null) {
                        cache.put(iconId, result);
                    }
                }
            } else {
                throw new IllegalArgumentException("Icon id isn't valid");
            }
        }
        return result;
    }

    public static void main(String... args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new GridFlowLayout());

        for (Object id : getInstance().iconURLs.keySet()) {
            JButton button = new JButton();
            button.setText(id.toString());
            button.setIcon(IconFactory.getIcon(id));
            frame.getContentPane().add(button);
        }
        frame.pack();
        frame.setVisible(true);
    }

}
