/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * SwingWidgetFactory.java
 *
 * Created: Nov 19, 2004
 *
 * @author Cédric Pineau <pineau@codelutin.com>
 * @version $Revision$
 *
 * Last update : $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

/**
 *
 */
public class SwingWidgetFactory {

    /**
     * Creates and returns a <code>AbstractButton</code>
     */
    public static AbstractButton createMenuButton(ApplicationAction action) {
        JButton button = new JButton(action);
        button.setText(null);
        return button;
    }

    /**
     * Creates and returns a <code>AbstractButton</code>
     */
    public static AbstractButton createButton(ApplicationAction action) {
        JButton button = new JButton(action);
        button.setText(action.getShortText());
        return button;
    }

    /**
     * Creates and returns a <code>JSplitPane</code>
     */
    public static JSplitPane createSplitPane() {
        return new UIFSplitPane();
    }

    /**
     * Creates and returns a <code>JPanel</code>
     */
    public static JPanel createPanel() {
        return new SimpleInternalFrame();
    }

    /**
     * Creates and returns a <code>JPanel</code>
     */
    public static JPanel createNamedPanel(String title) {
        return new SimpleInternalFrame(title);
    }

    public static void setPanelTitle(JPanel panel, String title) {
        if (panel instanceof SimpleInternalFrame) {
            ((SimpleInternalFrame) panel).setTitle(title);
        }
    }

    /**
     * Creates and returns a <code>JToolBar</code>
     */
    public static JToolBar createToolBar() {
        JToolBar toolBar = new JToolBar();

        toolBar.setFloatable(false);
        toolBar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);

        toolBar.setFloatable(false);
        Border marginBorder = new EmptyBorder(0, 2, 0, 2);
        EtchedBorder etchedBorder = new EtchedBorder();
        Border compoundBorder = new CompoundBorder(etchedBorder, marginBorder);
        toolBar.setBorder(compoundBorder);

        return toolBar;
    }

    /**
     * Creates and returns a <code>JMenuBar</code>
     */
    public static JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        Border marginBorder = new EmptyBorder(0, 2, 0, 2);
        EtchedBorder etchedBorder = new EtchedBorder();
        Border compoundBorder = new CompoundBorder(etchedBorder, marginBorder);
        menuBar.setBorder(compoundBorder);

        return menuBar;
    }

    public static JMenu createMenu(String text, char mnemonic) {
        JMenu menu = new JMenu(text);
        menu.setMnemonic(mnemonic);
        return menu;
    }

    public static JMenuItem createMenuItem(ApplicationAction action) {
        JMenuItem menuItem = new JMenuItem();
        menuItem.setAction(action);
        menuItem.setText(action.getShortText());
        return menuItem;
    }
}
