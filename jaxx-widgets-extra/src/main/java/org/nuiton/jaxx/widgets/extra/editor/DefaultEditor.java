/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * DefaultEditor.java
 *
 * Created: 6 août 2006 13:03:46
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra.editor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

/**
 * Default editor, can open of kind of file
 *
 * behaviour: <pre>undo/redo (Ctrl-z, Shift-Ctrl-z) </pre> Scrollbar
 *
 * @author poussin
 */
public class DefaultEditor extends JPanel implements EditorInterface,
        DocumentListener {

    /** serialVersionUID */
    private static final long serialVersionUID = 5049495816540748017L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(DefaultEditor.class);

    protected final JEditorPane editor = new JEditorPane();
    protected final JScrollPane scrollPane = new JScrollPane(editor);
    protected boolean isModified = false;

    public DefaultEditor() {
        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
        editor.getDocument().addDocumentListener(listener);
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
        editor.getDocument().removeDocumentListener(listener);
    }

    @Override
    public void addCaretListener(CaretListener listener) {
        editor.addCaretListener(listener);
    }

    @Override
    public void removeCaretListener(CaretListener listener) {
        editor.removeCaretListener(listener);
    }

    @Override
    public boolean accept(File file) {
        return true;
    }

    @Override
    public boolean accept(Editor.EditorSyntaxConstant editorSyntaxConstant) {
        return true;
    }

    @Override
    public boolean isModified() {
        return isModified;
    }

    @Override
    public void setSyntax(Editor.EditorSyntaxConstant editorSyntax) {
    }

    @Override
    public boolean open(File file) {
        try {
            Document doc = editor.getDocument();
            EditorHelper.removeUndoRedoSupport(editor);
            doc.removeDocumentListener(this);
            Reader in = new BufferedReader(new FileReader(file));
            editor.read(in, file);
            doc = editor.getDocument();
            doc.addDocumentListener(this);
            EditorHelper.addUndoRedoSupport(editor);
            isModified = false;
            return true;
        } catch (FileNotFoundException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't find file: " + file, eee);
            }
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't open file: " + file, eee);
            }
        }
        return false;
    }

    @Override
    public boolean saveAs(File file) {
        try {
            FileOutputStream outf = new FileOutputStream(file);
            Writer out = new OutputStreamWriter(outf, "utf-8");
            editor.write(out);
            isModified = false;
            return true;
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't save file", eee);
            }
        }
        return false;
    }

    @Override
    public String getText() {
        return editor.getText();
    }

    @Override
    public void setText(String text) {
        editor.setText(text);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        isModified = true;
    }

    @Override
    public void copy() {
        editor.copy();
    }

    @Override
    public void cut() {
        editor.cut();
    }

    @Override
    public void paste() {
        editor.paste();
    }
}
