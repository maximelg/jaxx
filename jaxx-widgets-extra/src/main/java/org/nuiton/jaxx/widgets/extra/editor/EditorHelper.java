/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.editor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.Document;
import javax.swing.text.TextAction;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * EditorHelper.
 *
 * Created: 6 août 2006 20:51:50
 *
 * @author poussin
 * @version $Revision$
 *
 *          Last update: $Date$
 *          by : $Author$
 */
public class EditorHelper {

    static final protected String UNDO_MANAGER = "UndoManager";

    static protected void addUndoRedoSupport(JEditorPane editor) {
        UndoManager undo = (UndoManager) editor.getClientProperty(UNDO_MANAGER);
        if (undo == null) {
            undo = new UndoManager();

            editor.putClientProperty(UNDO_MANAGER, undo);

            Action undoAction = new UndoAction(undo);
            KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_Z,
                                                   Event.CTRL_MASK);
            editor.getInputMap().put(key, "undo");
            editor.getActionMap().put("undo", undoAction);

            Action redoAction = new RedoAction(undo);
            key = KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK
                    + KeyEvent.SHIFT_DOWN_MASK);
            editor.getInputMap().put(key, "redo");
            editor.getActionMap().put("redo", redoAction);
        }
        Document doc = editor.getDocument();
        doc.addUndoableEditListener(undo);
    }

    static protected void removeUndoRedoSupport(JEditorPane editor) {
        UndoManager undo = (UndoManager) editor.getClientProperty(UNDO_MANAGER);
        if (undo != null) {
            Document doc = editor.getDocument();
            doc.removeUndoableEditListener(undo);
        }
    }

    static class UndoAction extends TextAction {
        /** serialVersionUID */
        private static final long serialVersionUID = 14313252664900665L;

        /** to use log facility, just put in your code: log.info(\"...\"); */
        static private final Log log = LogFactory.getLog(UndoAction.class);

        protected final UndoManager undo;

        /* Create this object with the appropriate identifier. */
        UndoAction(UndoManager undo) {
            super("undo");
            this.undo = undo;
        }

        /**
         * The operation to perform when this action is triggered.
         *
         * @param e the action event
         */
        public void actionPerformed(ActionEvent e) {
            try {
                if (undo.canUndo()) {
                    undo.undo();
                }
            } catch (CannotUndoException eee) {
                log.warn("Unable to undo: ", eee);
            }
        }
    }

    static class RedoAction extends TextAction {

        /** serialVersionUID */
        private static final long serialVersionUID = 5508420642118093156L;

        /** to use log facility, just put in your code: log.info(\"...\"); */
        static private final Log log = LogFactory.getLog(RedoAction.class);

        protected final UndoManager undo;

        /* Create this object with the appropriate identifier. */
        RedoAction(UndoManager undo) {
            super("redo");
            this.undo = undo;
        }

        /**
         * The operation to perform when this action is triggered.
         *
         * @param e the action event
         */
        public void actionPerformed(ActionEvent e) {
            try {
                if (undo.canRedo()) {
                    undo.redo();
                }
            } catch (CannotRedoException eee) {
                log.warn("Unable to redo: ", eee);
            }
        }
    }

}
