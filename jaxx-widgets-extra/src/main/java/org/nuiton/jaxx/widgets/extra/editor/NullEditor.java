/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.editor;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Editor used when the have no file to edit
 *
 * @author poussin
 * @version $Revision$
 *
 *          Last update: $Date$
 *          by : $Author$
 */
public class NullEditor extends JPanel implements EditorInterface {

    /** serialVersionUID. */
    private static final long serialVersionUID = -3451624841247752762L;

    public NullEditor() {
        setLayout(new BorderLayout());
        JLabel noFileLabel = new JLabel(t("nuitonwidgets.message.nofile"), JLabel.CENTER);
        noFileLabel.setFont(noFileLabel.getFont().deriveFont(Font.ITALIC));
        add(noFileLabel, BorderLayout.CENTER);
    }

    @Override
    public void addDocumentListener(DocumentListener listener) {
    }

    @Override
    public void removeDocumentListener(DocumentListener listener) {
    }

    @Override
    public void addCaretListener(CaretListener listener) {

    }

    @Override
    public void removeCaretListener(CaretListener listener) {

    }

    @Override
    public boolean accept(File file) {
        return false;
    }

    @Override
    public boolean accept(Editor.EditorSyntaxConstant editorSyntaxConstant) {
        return false;
    }

    @Override
    public void setSyntax(Editor.EditorSyntaxConstant editorSyntax) {
    }

    /**
     * when there is no file opened, the file is not modified.
     *
     * @return return false
     */
    @Override
    public boolean isModified() {
        return false;
    }

    /**
     * Do nothing, can't open file, on Null editor.
     *
     * @param file the file to open
     * @return this
     */
    @Override
    public boolean open(File file) {
        // nothing to do
        return true;
    }

    /**
     * Do nothing, can't save file, on Null editor
     *
     * @param file the file to open
     * @return this
     */
    @Override
    public boolean saveAs(File file) {
        // nothing to do
        return true;
    }

    @Override
    public String getText() {
        return "";
    }

    @Override
    public void setText(String text) {
    }

    @Override
    public void copy() {

    }

    @Override
    public void cut() {

    }

    @Override
    public void paste() {

    }
}
