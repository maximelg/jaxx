/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * SwingUtil.java
 *
 * Created: Nov 19, 2004
 *
 * @author Cédric Pineau <pineau@codelutin.com>
 * @version $Revision$
 *
 * Last update : $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import javax.swing.JFrame;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

/**
 *
 */
public class SwingUtil {

    public static void configureUI(Component component) {
        if (component instanceof JFrame) {
            ((JFrame) component)
                    .setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        }
        if (component instanceof Window) {
            ((Window) component).pack();
        }
        center(component);
    }

    /**
     * Center the widget on the screen
     */
    public static void center(Component component) {
        Toolkit tk = component.getToolkit();
        Dimension d = component.getSize();
        int x = (tk.getScreenSize().width - d.width) / 2;
        int y = (tk.getScreenSize().height - d.height) / 2;
        component.setLocation(x, y);
    }

}
