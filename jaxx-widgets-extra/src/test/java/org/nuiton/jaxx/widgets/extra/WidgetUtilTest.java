/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * WidgetUtilTest.java
 *
 * Created: Jul 29, 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.JLabel;
import java.awt.Component;
import java.awt.GraphicsEnvironment;

public class WidgetUtilTest { // WidgetUtilTest

    @BeforeClass
    public static void setUpClass() throws Exception {

        Assume.assumeTrue("Can't start test with headless env", !GraphicsEnvironment.isHeadless());

    }

    @Test
    public void testMakeDeepCopy() throws Exception {

        JLabel label = new JLabel("CodeLutin");
        Component component = WidgetUtil.makeDeepCopy(label);

        // test prouve que les composants ne sont pas les memes en profondeur
        Assert.assertTrue(component != label);
        Assert.assertTrue(component instanceof JLabel);
        Assert.assertEquals(label.getText(), ((JLabel) component).getText());
    }

} // WidgetUtilTest

