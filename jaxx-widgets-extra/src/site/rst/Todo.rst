.. -
.. * #%L
.. * JAXX :: Extra Widgets
.. * %%
.. * Copyright (C) 2004 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

TODO
====

ApplicationMonitor :
- Mettre des regexeps pour changer les paths de classes
- Mettre des choses utiles dans les panneaux
  - system : OS, version de la jvm, résolution de l'ecran, classpath, date, locale
  - application : version, mémoire utilisée par la jvm, uptime de l'appli ;-) ?
- Ajouter un constructeur qui donne en plus un StatusBar existant (dans ce cas, on ne construit pas le notre)

StatusBar :
- Ajouter une progressBar à droite toute, et peut-être un témoin d'activité (rouge/busy, vert/ready)

SplashScreen :
- Finir celui commencé
- Doit intégrer une forme de progressBar (utilisée en callback par l'application lancée)

Generateur d'interface de saisie de Bean:
- permettre de mettre un bean en paramètre de la classe, par introspection
  les champs sont lu et des zones de saisie sont généré. De cette façon
  on peut mettre des information simplement dans un Bean.
- prévoir des plugins pour mettre des éditeurs spécifique pour certaine
  propriété, peut-être regarder du coté des PropertyEditor ?

La modification d'interface a la CSS:
- Le but est d'avoir un fichier xml contenant les objets, avec leurs noms 
  leur event, et peut-etre des propriétés par defaut. Puis un fichier de
  type CSS, qui permette de modifier la disposition, la couleur, ...
  des objets. 
- un parser CSS:
  http://ajax.sourceforge.net/cypress/
- un layout se mariant bien avec le CSS:
  http://pnuts.org/snapshot/latest/modules/pnuts.awt/doc/PnutsLayout.html
  http://chrriis.brainlex.com/projects/uihierarchy/index.html
