<!--
  #%L
  JAXX :: Widgets Gis
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<Table id="coordinateEditorTopPanel" insets="0">

  <import>
    org.nuiton.jaxx.runtime.swing.CardLayout2Ext
    org.nuiton.jaxx.widgets.gis.CoordinateFormat
    java.io.Serializable
  </import>

  <script><![CDATA[
public void init() { handler.init(this); }
public void setBean(Serializable bean) { model.setBean(bean); }
public void setPropertyQuadrant(String property) { model.setPropertyQuadrant(property); }
public void setPropertyLatitude(String property) { model.setPropertyLatitude(property); }
public void setPropertyLongitude(String property) { model.setPropertyLongitude(property); }
public void setQuadrant(Integer quadrant) { model.setQuadrant(quadrant); }
public void setLatitude(Float latitude) { model.setLatitude(latitude); }
public void setLongitude(Float longitude) { model.setLongitude(longitude); }
public void setLatitudeAndLongitude(Float latitude, Float longitude) { model.setLatitudeAndLongitude(latitude, longitude); }
public void setFormat(CoordinateFormat format) { model.setFormat(format); }
public void setShowResetButton(boolean showResetButton) { model.setShowResetButton(showResetButton); }
public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) { model.setDisplayZeroWhenNull(displayZeroWhenNull); }
public void setFillWithZero(boolean fillWithZero) { model.setFillWithZero(fillWithZero); }
public void resetModel() { handler.resetModel(); }
public void resetQuadrant() { handler.resetQuadrant(); }
]]>
  </script>

  <CoordinatesEditorModel id='model' initializer='getContextValue(CoordinatesEditorModel.class)'/>
  <AbsoluteCoordinateEditorModel id='latitudeModel' initializer='model.getLatitudeModel()'/>
  <AbsoluteCoordinateEditorModel id='longitudeModel' initializer='model.getLongitudeModel()'/>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true' bean='model'>
    <field name='quadrant' component='quadrantEditor'/>
    <field name='latitude' component='latitudeEditor'/>
    <field name='longitude' component='longitudeEditor'/>
  </BeanValidator>

  <CardLayout2Ext id='latitudeLayout' constructorParams='this, "latitudeEditor"'/>
  <CardLayout2Ext id='longitudeLayout' constructorParams='this, "longitudeEditor"'/>

  <row>
    <cell anchor="west">
      <JPanel id='quadrantEditor' layout="{new GridLayout(2,2,0,0)}">
        <JToggleButton id='quadrant4' styleClass="enabled"/>
        <JToggleButton id='quadrant1' styleClass="enabled"/>
        <JToggleButton id='quadrant3' styleClass="enabled"/>
        <JToggleButton id='quadrant2' styleClass="enabled"/>
      </JPanel>
    </cell>
    <cell fill="both" weightx='1'>
      <Table id='coordinatesPanel' insets="0">
        <row>
          <cell anchor='east'>
            <JLabel id='latitudeLabel'/>
          </cell>
          <cell fill="both" weightx='1'>
            <JPanel id='latitudeEditor'>
              <AbsoluteDdCoordinateEditor id='latitudeDd' constraints='CoordinateFormat.dd.name()'
                                          styleClass="enabled"/>
              <AbsoluteDmsCoordinateEditor id='latitudeDms' constraints='CoordinateFormat.dms.name()'
                                           styleClass="enabled"/>
              <AbsoluteDmdCoordinateEditor id='latitudeDmd' constraints='CoordinateFormat.dmd.name()'
                                           styleClass="enabled"/>
            </JPanel>
          </cell>
        </row>
        <row>
          <cell anchor='east'>
            <JLabel id='longitudeLabel'/>
          </cell>
          <cell fill="both" weightx='1'>
            <JPanel id='longitudeEditor'>
              <AbsoluteDdCoordinateEditor id='longitudeDd' constraints='CoordinateFormat.dd.name()'
                                          styleClass="enabled"/>
              <AbsoluteDmsCoordinateEditor id='longitudeDms' constraints='CoordinateFormat.dms.name()'
                                           styleClass="enabled"/>
              <AbsoluteDmdCoordinateEditor id='longitudeDmd' constraints='CoordinateFormat.dmd.name()'
                                           styleClass="enabled"/>
            </JPanel>
          </cell>
        </row>
      </Table>
    </cell>
    <cell>
      <JPanel id='formatPanel' layout='{new GridLayout(0,1,0,0)}'>
        <JRadioButton id='dmsFormat'/>
        <JRadioButton id='dmdFormat'/>
        <JRadioButton id='ddFormat'/>
      </JPanel>
    </cell>
  </row>
</Table>
