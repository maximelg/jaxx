# jaxx changelog

 * Author [Nuiton team](mailto:jaxx-users@list.nuiton.org)
 * Last generated at 2017-03-11 12:20.

## Version [3.0-alpha-6](https://gitlab.nuiton.org/nuiton/jaxx/milestones/119)

**Closed at 2017-03-11.**


### Issues
  * [[Anomalie 1904]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1904) **[BeanComboBox] selectedItem property is not fired when selection changed in combobox** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [3.0-alpha-5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/118)

**Closed at 2017-03-08.**


### Issues
  * [[Evolution 1902]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1902) **Be able to force the selected item in BeanComboBox (report from 2.x)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [3.0-alpha-4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/116)

**Closed at 2017-03-08.**


### Issues
  * [[Anomalie 1895]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1895) **Error in DMD gis editor, minutes are not ok** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1896]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1896) **use pom 10.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1900]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1900) **fix gis editors (report from 2.x branch)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1901]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1901) **Migrate to gitlab** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.42](https://gitlab.nuiton.org/nuiton/jaxx/milestones/115)
&#13;&#10;&#13;&#10;*(from redmine: created on 2017-03-03)*

**Closed at 2017-03-10.**


### Issues
  * [[Anomalie 1903]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1903) **[BeanComboBox] selectedItem property is not fired when selection changed in combobox.** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.41](https://gitlab.nuiton.org/nuiton/jaxx/milestones/114)
&#10;&#10;*(from redmine: created on 2017-01-23)*

**Closed at 2017-03-03.**


### Issues
  * [[Anomalie 1898]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1898) **[JAXX-Demo] jaxx.cssExtension property is missing** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1899]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1899) **Be able to force the selected item in BeanComboBox** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.40](https://gitlab.nuiton.org/nuiton/jaxx/milestones/113)
&#10;&#10;*(from redmine: created on 2017-01-20)*

**Closed at 2017-01-20.**


### Issues
  * [[Anomalie 1897]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1897) **Revert previous fix on DmdConverter** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.39](https://gitlab.nuiton.org/nuiton/jaxx/milestones/111)
&#10;&#10;*(from redmine: created on 2017-01-18)*

**Closed at 2017-01-18.**


### Issues
  * [[Anomalie 1891]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1891) **Error in DMD gis editor, minutes are not ok** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1892]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1892) **Use nuiton-validator 3.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1893]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1893) **change java level to 7** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1894]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1894) **use pom 10.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [3.0-alpha-3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/110)
&#10;&#10;*(from redmine: created on 2017-01-01)*

**Closed at 2016-01-01.**


### Issues
  * [[Anomalie 1881]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1881) **Can&#39;t open links with file ou http protocol in method SwingUtil.openLink** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1805]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1805) **Extract a minimal runtime module used in generation** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1882]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1882) **Introduce jaxx-runtime-swing-session module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1883]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1883) **Move some api from jaxx-runtime to jaxx-widgets-extra** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1884]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1884) **Introduce jaxx-runtime-swing-application module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1885]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1885) **Introduce jaxx-runtime-swing-nav module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1886]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1886) **Introduce jaxx-runtime-swing-wizard module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1887]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1887) **Introduce jaxx-runtime-swing-help module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1888]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1888) **Review widgets to use UIHandler and other Jaxx best pratices** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1889]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1889) **Generate generic handler if necessary** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [3.0-alpha-2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/109)
&#10;&#10;*(from redmine: created on 2016-12-31)*

**Closed at 2016-12-31.**


### Issues
  * [[Anomalie 1503]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1503) **Some tests are failing with jdk 7 VM Server** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1346]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1346) **Add a breadcrumb widget** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1872]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1872) **Remove deprecated API and remove jaxx-widgets module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1873]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1873) **Review default configuration** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1874]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1874) **Introduce jaxx-widgets-file module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1875]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1875) **Introduce jaxx-widgets-status** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1876]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1876) **Remove jaxx-application modules** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1877]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1877) **Introduce jaxx-widgets-font module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1878]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1878) **Introduce jaxx-widgets-i18n module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1879]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1879) **Introduce jaxx-widgets-error module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1880]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1880) **Introduce jaxx-widgets-hidor module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1730]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1730) **Review the demo application** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1766]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1766) **Reimplements the action API** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.38](https://gitlab.nuiton.org/nuiton/jaxx/milestones/108)
&#10;&#10;*(from redmine: created on 2016-12-24)*

**Closed at 2017-01-03.**


### Issues
  * [[Evolution 1890]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1890) **Use JTextPane to display HTML in dialog box** (Thanks to Benjamin Poussin) (Reported by Tony Chemit)

## Version [2.37](https://gitlab.nuiton.org/nuiton/jaxx/milestones/107)
&#10;&#10;*(from redmine: created on 2016-12-14)*

**Closed at 2016-12-24.**


### Issues
  * [[Evolution 1869]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1869) **Add a way to customize ConfigCallBackUI** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.36](https://gitlab.nuiton.org/nuiton/jaxx/milestones/106)
&#10;&#10;*(from redmine: created on 2016-12-10)*

**Closed at 2016-12-14.**


### Issues
  * [[Evolution 1868]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1868) **Be able to clean a CardLayout2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.26](https://gitlab.nuiton.org/nuiton/jaxx/milestones/105)
&#10;&#10;*(from redmine: created on 2015-08-27)*

**Closed at *In progress*.**


### Issues
  * [[Anomalie 1814]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1814) **Got some ui error while opening the ActionUI** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1815]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1815) **Having the choice of the wildcard character in the filterable combobox** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1816]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1816) **Add a new Decorator that permits to edit in a combobox from a map** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [3.0-alpha-1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/104)
&#10;&#10;*(from redmine: created on 2015-04-05)*

**Closed at 2016-12-30.**


### Issues
  * [[Anomalie 1397]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1397) **Cannot generate class extend JaxxObject** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1830]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1830) **Make webstart compatible with jdk 8** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1809]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1809) **Migrates package to *org.nuiton.jaxx*** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1870]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1870) **Improve initialization of generated UI** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1602]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1602) **Update jdk level to 1.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.35](https://gitlab.nuiton.org/nuiton/jaxx/milestones/101)
&#10;&#10;*(from redmine: created on 2016-10-26)*

**Closed at 2016-12-10.**


### Issues
  * [[Anomalie 1867]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1867) **For DMD editor, we should consider for minutes and degres that value 5 equals 50 and not 05** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1866]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1866) **Do not display UIHandler warning on abstract classes** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.33.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/100)
&#10;&#10;*(from redmine: created on 2016-10-13)*

**Closed at 2016-10-13.**


### Issues
  * [[Anomalie 1863]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1863) **Quadrant should not be computed when latitude or longitude is changed** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1864]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1864) **Quadrant reset is not effective** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.34](https://gitlab.nuiton.org/nuiton/jaxx/milestones/99)
&#10;&#10;*(from redmine: created on 2016-09-19)*

**Closed at 2016-10-26.**


### Issues
  * [[Evolution 1865]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1865) **Add fillWithZero on Coordinate editor** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.33](https://gitlab.nuiton.org/nuiton/jaxx/milestones/98)
&#10;&#10;*(from redmine: created on 2016-09-13)*

**Closed at 2016-09-19.**


### Issues
  * [[Anomalie 1858]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1858) **Bad reset of quadrant** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1861]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1861) **actionIcon is no more generated** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1857]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1857) **Use jcss extension instead of cc in jaxx widgets** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1859]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1859) **Add more config ui model constructors** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1860]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1860) **Sanitize model builder methods** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1862]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1862) **Add a more generic method SwingUtil.getIcon with a classifier** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.32](https://gitlab.nuiton.org/nuiton/jaxx/milestones/97)
&#10;&#10;*(from redmine: created on 2016-08-28)*

**Closed at 2016-09-13.**


### Issues
  * [[Evolution 1854]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1854) **Introduce a new module jaxx-widget-about + make possible to add as many as wanted tags from resources** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1855]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1855) **Update dependencies** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1856]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1856) **Update to nuitonpom 10.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.31](https://gitlab.nuiton.org/nuiton/jaxx/milestones/96)
&#10;&#10;*(from redmine: created on 2016-08-02)*

**Closed at 2016-08-28.**


### Issues
  * [[Anomalie 1852]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1852) **missing i18n keys** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1853]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1853) **Updates i18n to 3.5.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.30.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/95)
&#10;&#10;*(from redmine: created on 2016-08-01)*

**Closed at 2016-08-02.**


### Issues
  * [[Anomalie 1851]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1851) **Constructor raw body are no more generated** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.30](https://gitlab.nuiton.org/nuiton/jaxx/milestones/94)
&#10;&#10;*(from redmine: created on 2016-04-25)*

**Closed at 2016-08-01.**


### Issues
  * [[Anomalie 1848]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1848) **memory leak in ApplicationActionSwingWorker$TuttiActionTimerTask** (Thanks to Benjamin Poussin) (Reported by Tony Chemit)
  * [[Evolution 1812]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1812) **Add missing ids and style classes automatically to the css file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1847]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1847) **Be able to use java 8 lambda syntax at least in construtor&#39;s bodies** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1849]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1849) **Updates to nuitonpom 10.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1850]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1850) **updates to nuiton-utils 3.0-rc-13** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.28.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/93)
&#10;&#10;*(from redmine: created on 2016-01-20)*

**Closed at 2016-01-20.**


### Issues
  * [[Anomalie 1836]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1836) **If css file extension is changed, then jaxx files are not recompiled if only a css file changed** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.28.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/92)
&#10;&#10;*(from redmine: created on 2016-01-18)*

**Closed at 2016-01-18.**


### Issues
  * [[Anomalie 1835]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1835) **Regression in SimpleTimeEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.29](https://gitlab.nuiton.org/nuiton/jaxx/milestones/91)
&#10;&#10;*(from redmine: created on 2016-01-18)*

**Closed at 2016-04-27.**


### Issues
  * [[Anomalie 1838]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1838) **NumberEditor - The 0 and clear one buttons in the popup have no label** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1839]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1839) **NumberEditor - The 0 button is not enabled when it should be** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1841]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1841) **[SwingSession] Some hidden columns reappear (the last ones)** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1837]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1837) **Add a EnumEditor renderer based on a Map (keys are enum values, values are labels to render)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1840]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1840) **[BeanDoubleList] add predicates to handle the removeEnabled state** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1842]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1842) **Update nuitonpom to 10** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1843]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1843) **Upgrade maven to 3.3.9** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1844]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1844) **Update nuiton-version to 1.0-rc-2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1845]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1845) **Update nuiton-config to 3.0-rc-4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1846]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1846) **Update nuiton-utils to 3.0-rc-12** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.28](https://gitlab.nuiton.org/nuiton/jaxx/milestones/90)
&#10;&#10;*(from redmine: created on 2015-12-24)*

**Closed at 2016-01-18.**


### Issues
  * [[Anomalie 1829]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1829) **Make javadoc compatible with jdk 1.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1832]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1832) **Use nuiton-version** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1833]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1833) **Configure the extension of css files** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1834]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1834) **Use nuiton-config 3.0-rc-3** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.27](https://gitlab.nuiton.org/nuiton/jaxx/milestones/89)
&#10;&#10;*(from redmine: created on 2015-09-07)*

**Closed at 2015-12-24.**


### Issues
  * [[Evolution 1817]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1817) **[Simple Time Editor] When the user types 2 digits in the hour field, automatically focus the minute field** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1818]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1818) **Updates to nuitonpom 5.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1819]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1819) **Remove cobertura report in jaxx-compiler module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1820]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1820) **Update guava to 19.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1821]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1821) **Update commons-collections to 3.2.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1822]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1822) **update commons-collections4 to 4.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1823]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1823) **update nuiton-csv to 3.0-rc-5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1824]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1824) **update nuiton-utils to 3.0-rc-10** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1825]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1825) **Updates commons-lang3 to 3.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1826]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1826) **updates rsyntaxtextarea to 3.5.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1827]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1827) **update maven-verifier to 1.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1828]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1828) **Update plexus-utils to 3.0.22** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1831]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1831) **Can reuse last maven-javadoc-plugin** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.25](https://gitlab.nuiton.org/nuiton/jaxx/milestones/87)
&#10;&#10;*(from redmine: created on 2015-04-23)*

**Closed at 2015-08-27.**


### Issues
  * [[Evolution 1813]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1813) **Add a resetModel method on coordinate editor** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.24](https://gitlab.nuiton.org/nuiton/jaxx/milestones/84)
&#10;&#10;*(from redmine: created on 2015-03-23)*

**Closed at 2015-04-23.**


### Issues
  * [[Evolution 1807]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1807) **Parser only that is necessary in java files** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1808]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1808) **Allow to use Diamond in java files** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1810]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1810) **Be able to display zero in coordinate componant editors for null values** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1811]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1811) **Authorize component by their type in BlockingLayerUI** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.23.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/83)
&#10;&#10;*(from redmine: created on 2015-03-23)*

**Closed at 2015-03-23.**


### Issues
  * [[Anomalie 1806]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1806) **NumberEditor keyboard only produces 7 digit!** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.23](https://gitlab.nuiton.org/nuiton/jaxx/milestones/82)
&#10;&#10;*(from redmine: created on 2015-03-14)*

**Closed at 2015-03-22.**


### Issues
  * [[Anomalie 1797]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1797) **[UnifiedValidatorMessageWidget] error on validator registration** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1798]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1798) **[UnifiedValidatorMessageWidget] Error on clearValidators** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1803]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1803) **[AbsoluteCoordinateEditor] Bad round value when changing quadrant value** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1799]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1799) **[UnifiedValidatorMessageWidget] add a getDefaultRenderer method** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1800]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1800) **[JaxxFileChooser] ignore case in the path** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1801]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1801) **[AbsoluteCoordinateEditor] Improve navigation of a coordinate** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1802]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1802) **Introduce a API to navigate inside a JFormattedTextField using a mask** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1804]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1804) **Use nuitonpom 1.8.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.22](https://gitlab.nuiton.org/nuiton/jaxx/milestones/81)
&#10;&#10;*(from redmine: created on 2015-01-08)*

**Closed at 2015-01-16.**


### Issues
  * [[Evolution 1796]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1796) **Add a label on universe and selected lists** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.20.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/80)
&#10;&#10;*(from redmine: created on 2015-01-05)*

**Closed at 2015-01-05.**


### Issues
  * [[Anomalie 1794]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1794) **Sometimes error are not well displayed** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.21](https://gitlab.nuiton.org/nuiton/jaxx/milestones/79)
&#10;&#10;*(from redmine: created on 2015-01-05)*

**Closed at 2015-01-08.**


### Issues
  * [[Anomalie 1795]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1795) **Can&#39;t inheritates handler when using handler generated by Jaxx** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.20](https://gitlab.nuiton.org/nuiton/jaxx/milestones/78)
&#10;&#10;*(from redmine: created on 2014-12-31)*

**Closed at 2015-01-02.**


### Issues
  * [[Anomalie 1789]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1789) **NumberEditor model does not fire when bean is changed.** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1791]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1791) **NumberEditor does not deal well with signed number** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1792]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1792) **Keep caret position if a bad caracter is given in NumberEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1793]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1793) **NumberEditor does not accept Long or long number type** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1790]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1790) **Add more hooks on ApplicationTableModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.19](https://gitlab.nuiton.org/nuiton/jaxx/milestones/77)
&#10;&#10;*(from redmine: created on 2014-12-11)*

**Closed at 2015-12-29.**


### Issues
  * [[Anomalie 1788]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1788) **BlockerLayerUI[2] does not block MouseWheelEvent** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1787]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1787) **[TABLE FILTER] uncheck the &quot;all&quot; action when the user starts typing in the search field** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.18](https://gitlab.nuiton.org/nuiton/jaxx/milestones/76)
&#10;&#10;*(from redmine: created on 2014-11-16)*

**Closed at 2014-12-05.**


### Issues
  * [[Anomalie 1775]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1775) **[Table Filter] the &quot;CONTAINS&quot; filter does not work with the upper case words** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1777]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1777) **Fix absolute coordinate editor labels** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1675]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1675) **Introduce a new NumberEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1746]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1746) **Introduce datetime editor and time editor (ng) widget** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1776]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1776) **Introduce a new FilterableDoubleList widget** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1778]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1778) **Improve the dmd editor pattern** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1779]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1779) **Pass a predicate to BooleanCellRenderer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1780]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1780) **Improve gis editor layout** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1781]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1781) **Introduce new jaxx-widgets modules** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1782]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1782) **Introduces JTables** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1783]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1783) **Use nuiton-decorator 3.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1784]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1784) **Use rsyntaxtextarea 2.5.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1785]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1785) **Use guava 18.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1786]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1786) **Use junit 4.12** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.16.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/75)
&#10;&#10;*(from redmine: created on 2014-11-06)*

**Closed at 2014-11-06.**


### Issues
No issue.

## Version [2.17](https://gitlab.nuiton.org/nuiton/jaxx/milestones/74)
&#10;&#10;*(from redmine: created on 2014-11-06)*

**Closed at 2014-11-25.**


### Issues
  * [[Anomalie 1774]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1774) **Sometimes, the loading ui never closes** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1748]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1748) **Be able to select default value of options** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1749]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1749) **Add an action to reset to default value** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1768]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1768) **Add a default directory option in JaxxFileChooser** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1769]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1769) **Can save a file giving a starting directory** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1772]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1772) **Add a keepCurrentDirectory option in JaxxFileChooser** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1773]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1773) **Using best ui to diplay messages in actions** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1770]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1770) **Updates plexus-util to 3.0.21** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1771]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1771) **updates nuitonpom to 1.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.16](https://gitlab.nuiton.org/nuiton/jaxx/milestones/73)
&#10;&#10;*(from redmine: created on 2014-10-30)*

**Closed at 2014-11-06.**


### Issues
  * [[Anomalie 1767]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1767) **ActionListCellRenderer does not take accoun of enabled action state** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.15](https://gitlab.nuiton.org/nuiton/jaxx/milestones/72)
&#10;&#10;*(from redmine: created on 2014-10-03)*

**Closed at 2014-10-30.**


### Issues
  * [[Evolution 1765]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1765) **Introduce a new API to choose files** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1764]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1764) **Updates nuitonpom to 1.5 + fix site urls** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.14](https://gitlab.nuiton.org/nuiton/jaxx/milestones/71)
&#10;&#10;*(from redmine: created on 2014-09-26)*

**Closed at 2014-10-03.**


### Issues
  * [[Anomalie 1762]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1762) **Shotcuts for disable Button is alaways disable even if button is enabled** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1763]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1763) **ColorCellEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1760]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1760) **[Table filter] Enable to choose the popup default size** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1761]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1761) **Add a progressbar and a title to the splashscreen** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.13](https://gitlab.nuiton.org/nuiton/jaxx/milestones/70)
&#10;&#10;*(from redmine: created on 2014-09-11)*

**Closed at 2014-09-26.**


### Issues
  * [[Anomalie 1754]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1754) **Error message in action** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1757]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1757) **Value of number cell editor not save when clicking outside a table** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1758]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1758) **Number cell editor in edition on focus** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1700]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1700) **Add Shortcuts** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1747]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1747) **Define a global CSS file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1753]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1753) **Use a *validatorLabel* client property to translate field in unified validator table** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1755]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1755) **Create a table filter** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1756]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1756) **Traduction field in UnifiedValidatorMessageTableRenderer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1759]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1759) **Enable to set the BeanFilterableComboBox&#39;s and BeanComboBox&#39;s combobox&#39;s maximumRowCount** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1752]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1752) **Use nuitonpom 1.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.12](https://gitlab.nuiton.org/nuiton/jaxx/milestones/69)
&#10;&#10;*(from redmine: created on 2014-08-18)*

**Closed at 2014-09-11.**


### Issues
  * [[Anomalie 1750]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1750) **Can&#39;t save configuration if one option is null** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1751]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1751) **In configuration, the last option value is not saved if you do not select another cell before clicking on the save button** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1742]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1742) **Introduce a new module jaxx-widgets-gis** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1743]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1743) **Add more methods on JavaBeanObjectUtil and deprecates org.nuiton.jaxx.application.ApplicationDataUtil** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1744]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1744) **Improve error log when java parsing fails in css files.** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1745]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1745) **Block an action if it is alreay running** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.11](https://gitlab.nuiton.org/nuiton/jaxx/milestones/68)
&#10;&#10;*(from redmine: created on 2014-08-16)*

**Closed at 2014-08-25.**


### Issues
  * [[Anomalie 1731]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1731) **NPE when creating a new session and adding a component** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1734]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1734) **Bad message when using autoImportCss and use others css** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1732]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1732) **Improve org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler#quitScreen(boolean, boolean, java.lang.String, java.lang.String, javax.swing.Action)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1733]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1733) **Add more useful method on org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1735]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1735) **Improve error message when css are not well formed** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1736]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1736) **Remove the autoCssImport option** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1737]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1737) **Remove warning when a css is not associated to a jaxx file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1739]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1739) **Use nuiton-converter API instead of nuiton-utils** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1740]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1740) **Improve the I18n detection** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1741]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1741) **Use nuiton-utils 3.0-rc-7 and replace old version API** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.9.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/67)
&#10;&#10;*(from redmine: created on 2014-08-06)*

**Closed at 2014-08-06.**


### Issues
  * [[Anomalie 1709]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1709) **Jaxx demo does not works on site** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.10](https://gitlab.nuiton.org/nuiton/jaxx/milestones/66)
&#10;&#10;*(from redmine: created on 2014-08-06)*

**Closed at 2014-08-16.**


### Issues
  * [[Anomalie 1717]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1717) **When NumberCellEditor lost focus, the edition should be canceled** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1719]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1719) **When table model changed (fireTableDataChanged invocation), beans are not removed from validator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1729]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1729) **While init a UIAction on a button, the enabled state from button is lost** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1710]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1710) **Add more methods in org.nuiton.jaxx.application.swing.action.ApplicationActionEngine** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1711]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1711) **Be able to access to BeanComboBox and BeanFilterableComboBox models** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1712]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1712) **Add a new API to prevent to change of a selected item in a combo box** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1713]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1713) **Add a new method org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler#quitInternalScreen** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1714]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1714) **Add init methods form BeanComboBox in org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1715]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1715) **Add more suitable JavaBeanObject API** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1718]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1718) **Improve AbstractApplicationTableModel API** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1720]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1720) **Introduce JavaBeanObjectUtil class** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1721]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1721) **Use in AbstractApplicationTableModel the identifier propertyName as the default columnName** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1722]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1722) **Add RemoveablePropertyChangeListener class** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1723]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1723) **Improve the way to manage validators in application ui handler** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1724]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1724) **Introduce AbstractApplicationFormUIModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1725]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1725) **Improve AbstractApplicationUIHandler and deprecates some poor codes** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1726]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1726) **Create a unified API to listen both SimpleBeanValidator and ListBeanValidator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1727]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1727) **Add a jaxx.runtime.validator.swing.SwingListValidatorMessageWidget** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1728]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1728) **Improve loading of SwingSession file and allow also to change it** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.9](https://gitlab.nuiton.org/nuiton/jaxx/milestones/65)
&#10;&#10;*(from redmine: created on 2014-06-11)*

**Closed at 2014-08-06.**


### Issues
  * [[Anomalie 1695]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1695) **[BeanDoubleList] Add button is active even if the universe list is not empty** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1696]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1696) **[BeanDoubleList] Doubleclick on selected list raise IOOB exception** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1705]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1705) **method org.nuiton.jaxx.application.ApplicationIOUtil#readContent only return first line of file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1699]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1699) **Improve wizard API** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1656]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1656) **Update to commons-collections4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1697]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1697) **Migrate to Git** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1698]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1698) **Updates to nuitonpom and use new site layout** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1701]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1701) **Use new Version object from nuiton-utils** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1702]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1702) **Updates i18n to 3.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1703]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1703) **Updates nuiton-config to 3.0-alpha-4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1704]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1704) **Updates nuiton-utils to 3.0-rc-5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1706]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1706) **Updates nuiton-validator to 3.0-rc-2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1707]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1707) **Updates nuiton-csv to 3.0-rc-4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1708]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1708) **Updates eugene to 2.13** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.7](https://gitlab.nuiton.org/nuiton/jaxx/milestones/64)
&#10;&#10;*(from redmine: created on 2014-06-02)*

**Closed at 2014-06-11.**


### Issues
  * [[Anomalie 1693]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1693) **The properties showI18n, showClock and showMemoryStatus of StatusMesagePanel are not used** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.8.6](https://gitlab.nuiton.org/nuiton/jaxx/milestones/63)
&#10;&#10;*(from redmine: created on 2014-05-14)*

**Closed at 2014-06-02.**


### Issues
  * [[Anomalie 1662]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1662) **Error for option call back** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1691]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1691) **Throw an exception when can&#39;t read a SwingSession** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1690]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1690) **Enable the component resizer to resize only horizontally or vertically** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1692]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1692) **Updates mavenpom to 5.0.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/62)
&#10;&#10;*(from redmine: created on 2014-04-05)*

**Closed at 2014-05-14.**


### Issues
  * [[Evolution 1687]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1687) **[CustomTab] Add a button to close the custom tab** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1688]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1688) **Improve I18n.t detected in text field** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1689]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1689) **Permits to not auto acquire focus in Bean combobox** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/61)
&#10;&#10;*(from redmine: created on 2014-03-25)*

**Closed at 2014-03-25.**


### Issues
  * [[Anomalie 1686]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1686) **Avoid to get error if model is empty** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/60)
&#10;&#10;*(from redmine: created on 2014-03-25)*

**Closed at 2014-03-25.**


### Issues
  * [[Anomalie 1685]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1685) **[NumberEditor] Can&#39;t edit a number starting with 0** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/59)
&#10;&#10;*(from redmine: created on 2014-02-12)*

**Closed at 2014-03-17.**


### Issues
  * [[Anomalie 1679]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1679) **Release profile invoked twice when release:perform** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1681]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1681) **NPE in JTableState** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1682]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1682) **No handler binding anylonger generated** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1683]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1683) **The &quot;showToolTipText&quot; option is not used in DecoratorCellRenderer** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1451]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1451) **Merge nuiton-widgets into Jaxx project** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1678]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1678) **Save the sort orders for the JTables in the Swingsession** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1680]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1680) **updates mavenpom to 5.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/58)
&#10;&#10;*(from redmine: created on 2014-02-11)*

**Closed at 2014-02-12.**


### Issues
  * [[Anomalie 1674]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1674) **NumberEditor does not support Double modelType** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1676]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1676) **Add option to select in a FilterableComboBox with enter key  whene there is a unique resultSet** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1677]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1677) **Updates nuiton-validator to 3.0-rc-1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.8](https://gitlab.nuiton.org/nuiton/jaxx/milestones/57)
&#10;&#10;*(from redmine: created on 2014-01-26)*

**Closed at 2014-02-04.**


### Issues
  * [[Tâche 1665]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1665) **Remove jaxx-swing-action moduel from svn** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1666]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1666) **Introduce a simple jaxx application framework** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1667]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1667) **Use mavenpom 4.7** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1668]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1668) **updates nuiton-utils to 3.0-rc-2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1669]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1669) **updates i18n to 3.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1670]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1670) **updates nuiton-validator to 3.0-alpha-3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1671]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1671) **Updates plexus-utils to 3.0.17** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1672]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1672) **Updates nuiton-csv to 3.0-alpha-3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1673]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1673) **Updates eugene to 2.7.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.7](https://gitlab.nuiton.org/nuiton/jaxx/milestones/56)
&#10;&#10;*(from redmine: created on 2013-12-04)*

**Closed at 2014-01-12.**


### Issues
  * [[Anomalie 1664]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1664) **Can&#39;t get the correct component with GetCompopentAtPointVisitor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1663]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1663) **Updates mavenpom to 4.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.30](https://gitlab.nuiton.org/nuiton/jaxx/milestones/55)
&#10;&#10;*(from redmine: created on 2013-11-11)*

**Closed at 2013-11-12.**


### Issues
  * [[Evolution 1652]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1652) **Add a reset button on FileEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1650]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1650) **Updates mavenpom to 4.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1651]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1651) **improve jws launching** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.29](https://gitlab.nuiton.org/nuiton/jaxx/milestones/54)
&#10;&#10;*(from redmine: created on 2013-10-08)*

**Closed at 2013-10-25.**


### Issues
  * [[Evolution 1647]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1647) **[ConfigUI] Improve the shortLabel of an option** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1648]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1648) **Add some nice methods to select rows and columns from jtable** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1649]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1649) **Validate more than one bean property with one editor** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.28](https://gitlab.nuiton.org/nuiton/jaxx/milestones/53)
&#10;&#10;*(from redmine: created on 2013-10-02)*

**Closed at 2013-10-07.**


### Issues
  * [[Evolution 1644]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1644) **Can stop propagation of events in JaxxListModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1645]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1645) **[BeanDoublieList] Add a beforeFilter zone (to add some stuff from outised)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1646]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1646) **[BeanComboBox] Add methods to add and remove multiple items (improve performance)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.27](https://gitlab.nuiton.org/nuiton/jaxx/milestones/52)
&#10;&#10;*(from redmine: created on 2013-09-24)*

**Closed at 2013-09-30.**


### Issues
  * [[Anomalie 1639]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1639) **[BeanFilterableComboBox] Editor still open even if editor is disabled** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1640]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1640) **[BeanDoubleList] Be able to show or hide the selected list popup** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1642]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1642) **[BeanDoubleList] Be able to add more than once selected for agiven universe item** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1643]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1643) **[BeanDoublieList] Can add a second decorator form selected list** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1641]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1641) **Updates nuiton-decorator to 3.0-alpha-2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.26](https://gitlab.nuiton.org/nuiton/jaxx/milestones/51)
&#10;&#10;*(from redmine: created on 2013-09-19)*

**Closed at 2013-09-23.**


### Issues
  * [[Evolution 1636]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1636) **[BeanComboBox] Add a property to not sort data** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1638]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1638) **[BeanDoubleList] Can reorder selected items** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1637]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1637) **Updates mavenpom to 4.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.25](https://gitlab.nuiton.org/nuiton/jaxx/milestones/50)
&#10;&#10;*(from redmine: created on 2013-07-10)*

**Closed at 2013-08-23.**


### Issues
  * [[Anomalie 1635]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1635) **Do not use FileUtil defaultDirectory methods in FileChooserUtil class** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1633]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1633) **Can veto the change of a category** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1634]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1634) **[FileChooserUtil] Add a method to check if current directory is still the default one** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1630]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1630) **Use nuiton-decorator 3.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1631]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1631) **Use nuiton-config 3.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1632]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1632) **Use nuiton-utils 2.7.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.24](https://gitlab.nuiton.org/nuiton/jaxx/milestones/49)
&#10;&#10;*(from redmine: created on 2013-07-03)*

**Closed at 2013-07-10.**


### Issues
  * [[Anomalie 1627]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1627) **Regression due to #2731 (Can&#39;t use any longer Bean widget API with generic types)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1628]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1628) **Updates rsyntaxtextarea to 2.0.7** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1629]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1629) **Updates eugene to 2.6.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.23](https://gitlab.nuiton.org/nuiton/jaxx/milestones/48)
&#10;&#10;*(from redmine: created on 2013-06-04)*

**Closed at 2013-07-03.**


### Issues
  * [[Anomalie 1625]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1625) **Can&#39;t use any longer Bean widget API with generic types** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1624]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1624) **Introduce a SimpleBeanValidator table model** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1626]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1626) **Updates mavenpom to 3.4.12** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.22](https://gitlab.nuiton.org/nuiton/jaxx/milestones/47)
&#10;&#10;*(from redmine: created on 2013-05-30)*

**Closed at 2013-06-04.**


### Issues
  * [[Evolution 1622]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1622) **[SwingSession] enable to replace a component if its path already exists** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1623]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1623) **Synchronize data access in filterableComboBox model** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.21](https://gitlab.nuiton.org/nuiton/jaxx/milestones/46)
&#10;&#10;*(from redmine: created on 2013-05-27)*

**Closed at 2013-05-30.**


### Issues
  * [[Evolution 1620]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1620) **Should be able to specify the file where to save** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1621]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1621) **[Swing session] create a state for the BeanDoubleList** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.5.20](https://gitlab.nuiton.org/nuiton/jaxx/milestones/45)
&#10;&#10;*(from redmine: created on 2013-05-13)*

**Closed at 2013-05-27.**


### Issues
  * [[Anomalie 1619]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1619) **[NumberEditor] When the new model does not match the pattern, the last text set is not correct** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1616]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1616) **[BeanDoubleList] Add a shortcut to add the selected items and to remove them from the selected** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1615]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1615) **[Swing session] create a state for the BeanFilterableComboBox** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1617]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1617) **Updates mavenpom to 3.4.11** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1618]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1618) **Updates i18n to 2.5.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.19](https://gitlab.nuiton.org/nuiton/jaxx/milestones/44)
&#10;&#10;*(from redmine: created on 2013-04-22)*

**Closed at 2013-05-13.**


### Issues
  * [[Evolution 1614]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1614) **Add a JaxxRuntimeException to catch some special errors** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1610]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1610) **Updates swingx to 1.6.5-1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1611]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1611) **Updates maven-verifier to 1.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1612]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1612) **Updates eugene to 2.6.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1613]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1613) **Updates rsyntaxtextarea to 2.0.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.18](https://gitlab.nuiton.org/nuiton/jaxx/milestones/43)
&#10;&#10;*(from redmine: created on 2013-04-15)*

**Closed at 2013-04-22.**


### Issues
No issue.

## Version [2.5.17](https://gitlab.nuiton.org/nuiton/jaxx/milestones/42)
&#10;&#10;*(from redmine: created on 2013-04-08)*

**Closed at 2013-04-15.**


### Issues
  * [[Anomalie 1606]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1606) **JXTableSwingSessionState - Error if the number of columns is different from the saved state** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1607]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1607) **Updates mavenpom to 3.4.9** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1608]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1608) **Updates nuiton-utils to 2.6.12** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1609]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1609) **Updates eugene to 2.6.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.16](https://gitlab.nuiton.org/nuiton/jaxx/milestones/41)
&#10;&#10;*(from redmine: created on 2013-03-28)*

**Closed at 2013-04-08.**


### Issues
  * [[Anomalie 1604]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1604) **BeanFilterableComboBox - the text is not cleared on reset clicked** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1601]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1601) **SwingValidatorMessageWidget - add a addTableModelListener method** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1603]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1603) **Move the SwingSession from nuiton-widget** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1605]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1605) **Introduce API to chosse a file or a directory (moved from nuiton-utils#FileUtil)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.15](https://gitlab.nuiton.org/nuiton/jaxx/milestones/40)
&#10;&#10;*(from redmine: created on 2013-03-18)*

**Closed at 2013-03-28.**


### Issues
  * [[Evolution 1599]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1599) **BeanFilterableComboBox - remove filter text when an item is selected** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1600]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1600) **[BeanFilterableComboBox] add predicate filter management** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.5.14](https://gitlab.nuiton.org/nuiton/jaxx/milestones/39)
&#10;&#10;*(from redmine: created on 2013-03-12)*

**Closed at 2013-03-18.**


### Issues
  * [[Anomalie 1586]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1586) **BeanFilterableComboBox - error when the user presses &quot;enter&quot; after typing text** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1587]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1587) **BeanFilterableComboBox - the text is not reset when th selected item is set to null by binding** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1588]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1588) **BeanFilterableComboBox - Bug when removing an item, the UI freezes** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1589]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1589) **[ConfigUI] When opening never center on parent** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1594]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1594) **[ConfigUI] Can not edit an option of type *char*** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1559]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1559) **Mnemonic should be able to be i18nAble** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1590]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1590) **DecoratorTableCellRenderer - add tooltiptext with the value of the cell** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1591]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1591) **[ConfigUI] Deal with application config array type options** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1592]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1592) **[ConfigUI] Make possible to use ui not in a frame** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1593]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1593) **Get a copy of delegate list of JaxxDefaultListModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1595]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1595) **[JAXXHelper] Improve api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1596]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1596) **Add new API to scan components as a tree** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1597]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1597) **Add a new method to get a parent of a component of a certain type (using the Container#parent api)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1598]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1598) **Updates nuiton-utils to 2.6.11** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.13](https://gitlab.nuiton.org/nuiton/jaxx/milestones/38)
&#10;&#10;*(from redmine: created on 2013-03-11)*

**Closed at 2013-03-12.**


### Issues
  * [[Anomalie 1583]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1583) **The table cell editor in the config options is never used** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Anomalie 1585]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1585) **Bug in the BeanFilterableComboBoxHandler when the selected item is a String (not the same type as the bean type)** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1584]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1584) **Enable to use a particular table cell renderer for a config option** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.5.12](https://gitlab.nuiton.org/nuiton/jaxx/milestones/37)
&#10;&#10;*(from redmine: created on 2013-03-05)*

**Closed at 2013-03-11.**


### Issues
  * [[Anomalie 1575]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1575) **Fix NPE in ComponentResizer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1579]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1579) **When invoking fileEditor from a dialog with alwaysOnTopdoes not display in top** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1574]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1574) **Improve SwingValidatorMessageWidget (add close button)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1576]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1576) **Can merge helper ids into inpute files in generate-help-ids mojo** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1577]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1577) **Add a strict mode on generate-help-ids mojo** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1578]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1578) **Change the default generated help ids filenames** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1580]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1580) **Improve JAXXHelpBroker** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1581]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1581) **Make a combobox whose elements can be filtered** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1582]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1582) **Updates mavenpom to 3.4.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.11](https://gitlab.nuiton.org/nuiton/jaxx/milestones/36)
&#10;&#10;*(from redmine: created on 2013-02-15)*

**Closed at 2013-03-05.**


### Issues
  * [[Anomalie 1571]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1571) **NumberCellEditor - the textfield is not emptied when we change to an empty cell** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1567]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1567) **Enable to set a tooltip on the NumberEditor** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1568]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1568) **Add filter and sorted on the BeanDoubleList widget** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1570]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1570) **BeanComboBox - trigger the item selection only when the user really selects an item** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1569]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1569) **Updates to nuiton-utils 2.6.10** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1572]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1572) **Deprecates ConfigUI (using old applicationconfig api)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.10](https://gitlab.nuiton.org/nuiton/jaxx/milestones/35)
&#10;&#10;*(from redmine: created on 2013-01-04)*

**Closed at 2013-02-15.**


### Issues
  * [[Evolution 1560]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1560) **Create a button to show a popup with a validation table** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Evolution 1561]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1561) **Introduce a new comboBoxModel which does not use a Vector** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1562]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1562) **Introduce a new listModel which does not use a Vector** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1563]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1563) **Improve the DoubleListBean api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1564]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1564) **Add a sort model on BeanComboBoxModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1565]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1565) **Create an adapter to move a component by using the mouse.** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1566]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1566) **Create an adapter to resize a component by dragging a border of the component** (Thanks to Kevin Morin) (Reported by Tony Chemit)

## Version [2.5.9](https://gitlab.nuiton.org/nuiton/jaxx/milestones/34)
&#10;&#10;*(from redmine: created on 2012-12-15)*

**Closed at 2013-01-04.**


### Issues
  * [[Evolution 1551]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1551) **[BeanComboBox] add method to add and remove items from comboBox** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1552]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1552) **[BeanComboBox] add empty property** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1553]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1553) **[BeanComboBox] Use widget with no model** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1554]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1554) **Use the genericType to fill the beanType property on BeanTypeAware widgets** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1555]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1555) **Add BeanComboBox demo** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1556]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1556) **Set encoding to UTF-8 in jaxx-m-p tests** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1557]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1557) **[BeanDoubleList] Use widget with no model** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1558]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1558) **Updates rsyntaxtextarea to 2.0.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.8](https://gitlab.nuiton.org/nuiton/jaxx/milestones/33)
&#10;&#10;*(from redmine: created on 2012-11-23)*

**Closed at 2012-12-15.**


### Issues
  * [[Evolution 1546]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1546) **Create a double list widget** (Thanks to Kevin Morin) (Reported by Tony Chemit)
  * [[Tâche 1547]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1547) **Updates mavenpom to 3.4.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1548]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1548) **Updates to nuiton-utils 2.6.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1549]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1549) **Updates Eugene to 2.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1550]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1550) **Updates plexus-utils to 3.0.10** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.7](https://gitlab.nuiton.org/nuiton/jaxx/milestones/32)
&#10;&#10;*(from redmine: created on 2012-10-17)*

**Closed at 2012-11-23.**


### Issues
  * [[Anomalie 1541]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1541) **Can not use enabled binding on TabInfo** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1539]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1539) **Updates to mavenpom 3.4.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1540]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1540) **Updates to helper-m-p 2.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1542]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1542) **Updates to nuiton-utils 2.6.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1543]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1543) **Updates to eugene 2.5.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1544]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1544) **Use css for bean widgets** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1545]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1545) **Updates to plexus-util 3.0.9** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.6](https://gitlab.nuiton.org/nuiton/jaxx/milestones/31)
Improve css parsing&#10;&#10;*(from redmine: created on 2012-08-29)*

**Closed at 2013-11-28.**


### Issues
  * [[Anomalie 1655]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1655) **Does not compile if an interface extends JAXXObject** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1573]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1573) **Introduce a new jaxx-config module** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1653]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1653) **Introduce a new *Simple* time editor.** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1654]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1654) **Introduce some coordinate editors** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1657]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1657) **Introduce UIHandler contract and a way to glue it to any generated jaxx object** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1658]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1658) **Updates nuiton-i18n to 2.5.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1659]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1659) **Updates eugene to 2.7.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1660]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1660) **Updates plexus-utils to 3.0.15** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1661]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1661) **Updates rsyntaxtextarea to 2.5.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.6](https://gitlab.nuiton.org/nuiton/jaxx/milestones/30)
&#10;&#10;*(from redmine: created on 2012-08-23)*

**Closed at 2012-10-12.**


### Issues
  * [[Anomalie 1526]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1526) **SwingListValidatorMessageTableModel is not correct** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1535]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1535) **Can not use in a jaxx file some java file as soon they extends some other classes** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1537]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1537) **Can no use css to add title in tab** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1527]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1527) **Improve FileEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1536]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1536) **Improve ConfigUIModelBuilder api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1528]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1528) **Updates to i18n-m-p 2.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1529]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1529) **Updates to nuiton-utils 2.6.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1530]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1530) **Updates to eugene 2.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1531]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1531) **Updates to maven-plugin-testing-harness 1.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1532]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1532) **Updates to mavenpom 3.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1533]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1533) **Updates to plexus-util 3.0.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1534]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1534) **Updates to rsyntaxtextarea 1.5.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/29)
&#10;&#10;*(from redmine: created on 2012-08-08)*

**Closed at 2012-08-23.**


### Issues
  * [[Anomalie 1522]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1522) **Make possible again to change SwingValidatorFactory** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1523]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1523) **Fix pom dependencies** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1462]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1462) **Remove deprecated decorator api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1521]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1521) **Updates to nuiton-utils 2.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/28)
&#10;&#10;*(from redmine: created on 2012-07-30)*

**Closed at 2012-08-08.**


### Issues
  * [[Anomalie 1512]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1512) **NPE in ConfigUI if one option has a nul value** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1517]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1517) **The Class type in configuration is not properly saved** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1520]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1520) **ClassCellEditor does not work for anything but classes** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1510]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1510) **Improve ConfigUI** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1513]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1513) **Do not show sort property to change if there is only one property available in BeanUI api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1515]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1515) **Make a color editor and its renderer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1518]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1518) **Improve ConfigUI to use configuration with no inheritanceon ApplicationConfig** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1519]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1519) **Introduce a ClassTableCellRenderer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1511]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1511) **Add a logback configuration file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1514]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1514) **Updates to swingx 1.6.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1516]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1516) **updates to nuiton-utils 2.5.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/27)
&#10;&#10;*(from redmine: created on 2012-07-17)*

**Closed at 2012-07-30.**


### Issues
  * [[Anomalie 1505]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1505) **SwingUtil.editCell method does not work properly** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1507]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1507) **NPE when no file selected within the FileCellEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1504]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1504) **Use new validation api from nuiton-utils** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1506]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1506) **Add new methods in SwingUtil** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1508]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1508) **Add method in SwingUtil to auto-select a cell content in a table when the cell is selected** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1509]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1509) **Can auto-select NumberEditor content when having bad content** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1502]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1502) **Updates to nuiton-utils 2.5.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/26)
&#10;&#10;*(from redmine: created on 2012-07-10)*

**Closed at 2012-07-17.**


### Issues
  * [[Anomalie 1501]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1501) **Null management with NumberCellEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1500]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1500) **Update ListToListSelector component** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1499]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1499) **Update to swingx 1.6.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/25)
&#10;&#10;*(from redmine: created on 2012-07-02)*

**Closed at 2012-07-10.**


### Issues
  * [[Anomalie 1484]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1484) **ClassDescriptor equals and isAssignableFrom are not correct** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1485]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1485) **Jaxx deos not compile under jdk 7 (field comparator fails)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1488]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1488) **Can&#39;t use &#39;errors&#39; id for SwingValidatorMessageTableModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1495]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1495) **FileEditor dont keep input path** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1401]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1401) **Use maven-plugin-plugin 3 api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1487]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1487) **Create GenericListSelectionModel extracted from GenericListModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1490]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1490) **Improve ActionWorker api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1496]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1496) **Add keyStroke editor to input shortcut key in config ui** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1489]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1489) **Updates to commons-lang3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1491]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1491) **Updates to mavenpom 3.3.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1492]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1492) **Updates to eugene 2.4.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1493]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1493) **Updates to maven-verifier 1.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1494]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1494) **Updates to plexus-utils 3.0.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1497]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1497) **Updates to nuiton-utils 2.5.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1498]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1498) **Remove old tutorials** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/24)
&#10;&#10;*(from redmine: created on 2011-05-15)*

**Closed at 2012-07-02.**


### Issues
  * [[Anomalie 1468]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1468) **Can not compile jaxx demo module with jdk 1.7** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1469]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1469) **Can not use any more menu item with no href with m-site-p 3.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1470]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1470) **inherited attribute no more supported by item of site.xml** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1480]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1480) **Can not build release application** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1483]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1483) **Jnlp demo won&#39;t works.** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1459]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1459) **Move decorator neutral api to nuiton-utils** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1464]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1464) **treat imageIcon as icon and actionIcon** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1467]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1467) **Change maven-jaxx-plugin to jaxx-maven-plugin** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1473]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1473) **Allow to specify date pattern to display for ClockWidget** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1474]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1474) **Create component extend JXDatePicker to hide popup button and allow input time** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1475]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1475) **Add component to choose a file** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1476]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1476) **Use FileEditor to input File in ApplicationConfigUI** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1477]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1477) **Add method to select next editable cell on TAB key released** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1478]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1478) **Add numberCellEditor using NumberEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1479]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1479) **Add widget to select values in list** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1460]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1460) **Updates to nuiton-utils 2.5** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1463]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1463) **Updates to i18n 2.4.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1465]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1465) **Updates to helper-m-p 1.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1466]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1466) **Update to xworks-core 2.3.1.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1471]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1471) **Updates to mavenpom 3.3.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1481]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1481) **Use now fluido for generated site** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Tâche 1482]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1482) **Remove tutorial as part of release build** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.4.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/23)
&#10;&#10;*(from redmine: created on 2011-04-15)*

**Closed at 2011-05-15.**


### Issues
  * [[Anomalie 1445]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1445) **Non public classes make generation fails** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1452]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1452) **Loading java source file does not take account of super classes** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1454]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1454) **[Binding regression] Binding on javaBean doesn&#39;t seams to work** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1440]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1440) **Update to rSyntaxTextArea 1.5.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1455]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1455) **Integrates spanish i18n translations** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1456]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1456) **Updates to nuiton-utils 2.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1457]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1457) **Use Clonable contract to create new decorators** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1458]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1458) **Updates to i18n 2.4 (using utf-8 i18n files)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.4.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/22)
&#10;&#10;*(from redmine: created on 2011-03-02)*

**Closed at 2011-04-15.**


### Issues
  * [[Anomalie 1439]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1439) **Improve log in JXPathDecorator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1441]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1441) **Using decorator&#61;&#39;boxed&#39; or icon&#61;&quot;img.png&quot; miss import jaxx.runtime.SwingUtil** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1443]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1443) **NPE when cloning method with arguments** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1448]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1448) **Jaxx should not be embed validators.xml file at runtime** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1449]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1449) **Can&#39;t use &lt;SwingValidator context&#61;&quot;myContext&quot;&gt;** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1450]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1450) **Warnings are not displayed** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1442]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1442) **Improve import manager usage** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1444]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1444) **Can simplify body code of method while cloning it** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1446]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1446) **Can set a numberPattern on number editor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1447]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1447) **Updates to nuiton-utils 2.1.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/20)
&#10;&#10;*(from redmine: created on 2011-01-27)*

**Closed at 2011-02-20.**


### Issues
  * [[Anomalie 1411]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1411) **call constructors other than default ones** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1436]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1436) **Java file parsing is done twice** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1403]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1403) **Updates to commons-io 2.0.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1427]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1427) **Updates to mavenpom 2.5.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1428]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1428) **Improve generation code** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1429]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1429) **Updates to i18n 2.3.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1430]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1430) **Updates to JXLayer 3.0.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1431]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1431) **Updates to rsyntaxtextarea R239 a.k.a version 1.4.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1432]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1432) **Updates to javaHelp 2.0.05** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1433]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1433) **Updates to plexus-velocity 1.1.8** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1434]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1434) **Introduce a JXLayerHandler to deal with new api of JXLayer** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1435]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1435) **Improve DefaultFinalizer code** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1437]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1437) **Improve ClassDescriptor loading (try also to obtain constructors if possible)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1438]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1438) **Replace StringBuffer by StringBuilder** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2.5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/19)
Improve Nav api&#10;&#10;*(from redmine: created on 2011-01-01)*

**Closed at 2010-01-01.**


### Issues
  * [[Anomalie 1416]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1416) **Improve nav api** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/18)
Use nuiton-validator&#10;&#10;*(from redmine: created on 2010-12-30)*

**Closed at 2011-01-27.**


### Issues
  * [[Anomalie 1420]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1420) **Memory leak when changing validation context** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1413]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1413) **Use nuiton-validator 2.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1417]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1417) **Can skip null value in CollectionUniqueKeyValidator validator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1418]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1418) **Updates mavenpom to 2.4.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1419]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1419) **Updates nuiton-i18n to 2.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1421]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1421) **Can generate annotations on fields or methods** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1422]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1422) **Updates nuiton-utils to 2.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1423]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1423) **Add getField method on JavaFile** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1424]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1424) **Add a validation tutorial** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1425]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1425) **Review the validation api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1426]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1426) **Deprecate the validatorFQN in mojo** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2.4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/17)
&#10;&#10;*(from redmine: created on 2010-10-21)*

**Closed at 2010-12-30.**


### Issues
  * [[Anomalie 1409]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1409) **Runtime exception when using javax.swing.event.TableColumnModelListener#columnMarginChanged(ChangeEvent)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1404]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1404) **Add new &#39;fatal&#39; message level in bean validation** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1405]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1405) **Use new maven-license-plugin mecanism to update file headers** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1406]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1406) **Optimize dependencies** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1407]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1407) **Update i18n to 2.0.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1408]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1408) **Update nuiton-utils to 1.5.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1414]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1414) **Introduce a new module jaxx-validator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1415]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1415) **Deprecates the jaxx validator api** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2.3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/16)
&#10;&#10;*(from redmine: created on 2010-09-30)*

**Closed at 2010-10-21.**


### Issues
  * [[Evolution 1398]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1398) **Remove deprecated EntityComboBoxHandler** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1399]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1399) **Update swingx to swingx-core 1.6.2-2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1400]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1400) **Remove deprecated api from 2.0.2 (JavaFileParser and ClassDescriptorLoader)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1402]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1402) **Add method to find child by id in tree nav api** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/15)
&#10;&#10;*(from redmine: created on 2010-09-16)*

**Closed at 2010-09-30.**


### Issues
  * [[Anomalie 1396]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1396) **Propager les bons évènements pour les TreeTables** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/14)
&#10;&#10;*(from redmine: created on 2010-09-07)*

**Closed at 2010-09-16.**


### Issues
  * [[Anomalie 1394]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1394) **Validator fields are not refresh when validation changes his context name** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1395]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1395) **Improve BeanListHeader (can reset selection)** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/13)
&#10;&#10;*(from redmine: created on 2010-07-31)*

**Closed at 2010-09-07.**


### Issues
  * [[Anomalie 1380]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1380) **Validators does not translate error message for none trim message** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1387]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1387) **Can not use auto popup if showPopup is false** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1392]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1392) **when using MultiJxPathDecorator, can not use extra token formatter in expression** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1377]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1377) **Remove deprecated navigation api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1378]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1378) **Can inject a TreeWillExpandListener to NavHelper** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1379]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1379) **Remove deprectaed AbstractActionThread** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1381]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1381) **Add a closeAction in ConfigUI api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1382]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1382) **Update swingx to 1.6.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1383]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1383) **Update nuiton-utils to 1.4.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1384]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1384) **Update mavenpom4redmine to 2.2.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1385]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1385) **Improve TimeEditor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1386]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1386) **Improve jaxx demo** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1388]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1388) **Introduce jaxx.runtime.swing.editor.bean package** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1389]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1389) **Deprecates EntityComboBox, use now BeanComboBox** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1390]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1390) **Can sort in reverse order in DecoratorUtils** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1391]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1391) **Introduce BeanListHeader** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1393]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1393) **Can add the columnHeaderView component in the tag JScrollPane** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.1.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/12)
&#10;&#10;*(from redmine: created on 2010-07-02)*

**Closed at 2010-07-31.**


### Issues
  * [[Evolution 1376]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1376) **Improve multi-selection in tree api** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.2](https://gitlab.nuiton.org/nuiton/jaxx/milestones/11)
&#10;&#10;*(from redmine: created on 2010-03-31)*

**Closed at 2010-05-11.**


### Issues
  * [[Anomalie 1345]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1345) **Can not use jaxx widgets in css (property autoRecurseInCss&#61;false)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1282]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1282) **Improve compiler design** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1335]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1335) **Regenerates jaxx files when his css file is modified** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1336]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1336) **Use I18n 1.2.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1337]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1337) **Use mavenpom4redmine 2.1.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1338]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1338) **Use maven-license-plugin 2.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1339]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1339) **Use nuiton-utils 1.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1340]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1340) **Use junit 4.8.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1341]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1341) **Improve ConfigUI api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1342]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1342) **Remove jaxx-swing-action from part of build** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1343]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1343) **module jaxx-demo is only build at release time** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1344]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1344) **Add Auto import of css mode in generate goal (property autoImportCss&#61;true)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1349]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1349) **Use maven-helper-plugin 1.2.4** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1350]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1350) **Use log4j 1.2.16** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/10)
&#10;&#10;*(from redmine: created on 2010-03-13)*

**Closed at 2010-03-31.**


### Issues
  * [[Evolution 1297]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1297) **Remove deprecated api from version 2.0** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1328]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1328) **Introduce a extented jaxx.runtime.decorator.MapPropertyHandler** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1329]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1329) **Pouvoir trier les nœuds des arbres de navigations** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1330]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1330) **Can listen modifications of a DefaultApplicationContext via the PCS api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1331]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1331) **Refactor navigation package (a abstract based + a impl package for tree and treetable)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1332]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1332) **Add methods in JAXXUtil to clean listeners** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1333]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1333) **Using nuiton-i18n 1.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1334]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1334) **Using mavenpom 2.0.7** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.0-beta-6](https://gitlab.nuiton.org/nuiton/jaxx/milestones/9)
&#10;&#10;*(from redmine: created on 2010-02-08)*

**Closed at 2010-03-06.**


### Issues
  * [[Evolution 1301]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1301) **Can build EnumEditor on restricted values of enum universe** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1302]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1302) **Mae Navigation api compatible with TreeTable widget from SwingX framework** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1303]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1303) **Improve editable and focusable properties on EntityComboBox** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/8)
&#10;&#10;*(from redmine: created on 2010-01-29)*

**Closed at 2010-07-02.**


### Issues
  * [[Anomalie 1351]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1351) **Fix logging issues in plugin (since nuiton-utils 1.3)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1352]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1352) **Fix release of demo and tutorials** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1353]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1353) **Improve error when fail to load a script** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1354]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1354) **Make script parse correctly field declaration with generics** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1355]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1355) **Register bindings when all components are initialized** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1356]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1356) **Fix broken dependency jaxx files mecanism** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1357]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1357) **Interface methods are not loaded in ClassDescriptor** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1358]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1358) **Improve data binding with inheritance** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1359]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1359) **generate generics on overriden object** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1360]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1360) **Introduce jaxx.runtime.swing.layer package** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1361]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1361) **BlockingLayerUI can accept some components NOT to be blocked** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1362]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1362) **Add logger on each generated ui (with inheritance or not)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1363]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1363) **Add helloworld tutorial** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1364]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1364) **Add a data-binding tutorial** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1365]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1365) **Add a CSS tutorial** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1366]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1366) **Use nuiton-utils 1.3.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1367]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1367) **Replace too complex WizardOperationModel with WizardExtModel** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1368]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1368) **Introduce a new tree api to replace too complex old one** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1370]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1370) **Add import tag** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1371]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1371) **Use rsyntaxtextarea 1.4.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1372]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1372) **Use mavenpom 2.2.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1373]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1373) **Use maven-helper-plugin 1.2.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1374]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1374) **Use i18n 1.2.3** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1375]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1375) **Use nuiton-utils 1.3.2** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.0-beta-5](https://gitlab.nuiton.org/nuiton/jaxx/milestones/7)
&#10;&#10;*(from redmine: created on 2010-01-07)*

**Closed at 2010-02-08.**


### Issues
  * [[Anomalie 1295]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1295) **La generation de l&#39;index de l&#39;aide n&#39;est pas correct** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1299]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1299) **Fix NPE in Decorator sort when context data is null** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1296]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1296) **Renomer Util en JAXXUtil** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1298]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1298) **Improve ConfigUI with a CallBackManager** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.0-beta-4](https://gitlab.nuiton.org/nuiton/jaxx/milestones/6)
&#10;&#10;*(from redmine: created on 2009-12-23)*

**Closed at 2010-01-07.**


### Issues
  * [[Anomalie 1292]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1292) **Bug dans constructorParams** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1293]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1293) **Bump jx dependencies versions** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1294]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1294) **Use maven-helper-plugin 1.2.0 and revisited AbstractPlugin api** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.0-beta-3](https://gitlab.nuiton.org/nuiton/jaxx/milestones/5)
&#10;&#10;*(from redmine: created on 2009-12-21)*

**Closed at 2009-12-23.**


### Issues
  * [[Anomalie 1269]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1269) **Condition !&#61; null manquante sur les bindings d&#39;interface** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1289]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1289) **NumberEditor avec un Double ou BigDecimal** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1281]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1281) **Améliorer le code généré** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1287]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1287) **Ajout de constantes sur les propriétés javaBeans générées** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1290]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1290) **Générer les index de l&#39;aide dans un goal spécific** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1291]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1291) **Amélioration de l&#39;ui de configuration** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0](https://gitlab.nuiton.org/nuiton/jaxx/milestones/4)
&#10;&#10;*(from redmine: created on 2009-11-26)*

**Closed at 2010-03-13.**


### Issues
  * [[Anomalie 1305]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1305) **NPE on NavigationTreehelper due to multi selection** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1322]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1322) **Bug dans constructorParams** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1323]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1323) **Fix NPE in Decorator sort when context data is null** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1325]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1325) **NumberEditor avec un Double ou BigDecimal** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1304]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1304) **Use nuiton-utils 1.2 and nuiton-i18n 1.1** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1306]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1306) **Use mavenpom4redmine 2.0.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1307]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1307) **Use swingx 1.6** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1308]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1308) **simplifier les modules maven + reorganisation des paquetages** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1309]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1309) **Refactor binding framework** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1310]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1310) **Use maven-helper-plugin 1.2.0 and revisited AbstractPlugin api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1311]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1311) **Refactor Navigation framework** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1312]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1312) **Enable use of generics on objects** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1313]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1313) **Can generates simple Swing class and not direclty JAXX object (JTree, JComboBox,...)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1314]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1314) **Simplify maven modules** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1315]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1315) **Make help framework multi-language** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1316]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1316) **Make JAXXObject serializable** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1317]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1317) **Allows initialization of object wich are not javabeans** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1318]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1318) **Improve decorator api** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1319]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1319) **Improve generated code** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1320]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1320) **Generates constants for binding and javabeans properties** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1321]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1321) **Improve UI configuration** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1324]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1324) **Rename jaxx.runtime.Util to jaxx.runtime.JAXXUtil** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1326]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1326) **Can build EnumEditor on restricted values of enum universe** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1327]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1327) **Generates help index in a specific goal** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [2.0.0-beta-1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/3)
refactor du runtime&#10;&#10;*(from redmine: created on 2009-10-06)*

**Closed at 2009-11-13.**


### Issues
  * [[Anomalie 1274]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1274) **Regression dans JAXXComboBox** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Anomalie 1285]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1285) **NPE sur un removeDataBinding** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1271]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1271) **utilisation du framework mojo maven-helper-plugin** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1272]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1272) **utilisation nouvelle configuration webstart** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1275]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1275) **pouvoir generer les classes swing (JComboBox, JList,...)** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1276]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1276) **Supprimer les codes non utilisés** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1277]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1277) **simplifier les modules maven + reorganisation des paquetages** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1279]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1279) **rendre le système d&#39;aide multi-langue** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1280]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1280) **les objets JAXX sont serializable** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1283]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1283) **permettre l&#39;initialisation des objets sans JavaBean** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1284]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1284) **Amélioration de l&#39;api des Decorator** (Thanks to Tony Chemit) (Reported by Tony Chemit)
  * [[Evolution 1286]](https://gitlab.nuiton.org/nuiton/jaxx/issues/1286) **Permettre d&#39;utiliser des generics sur tous les objets** (Thanks to Tony Chemit) (Reported by Tony Chemit)

## Version [1.7.0](https://gitlab.nuiton.org/nuiton/jaxx/milestones/2)
&#10;&#10;*(from redmine: created on 2009-09-29)*

**Closed at 2009-07-06.**


### Issues
No issue.

## Version [1.7.1](https://gitlab.nuiton.org/nuiton/jaxx/milestones/1)
&#10;&#10;*(from redmine: created on 2009-09-29)*

**Closed at 2008-08-25.**


### Issues
No issue.

