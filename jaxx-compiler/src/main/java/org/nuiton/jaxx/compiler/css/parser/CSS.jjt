/*
 * Copyright 2006 Ethan Nicholas. All rights reserved.
 * Use is subject to license terms.
 */
 
// I would love to have used an existing CSS parser, but all of the ones I could
// find are licensed under the LGPL.  As JAXX is BSD licensed and I'm not a big
// fan of the LGPL, unfortunately that won't work.
options {
  STATIC = false;
  JDK_VERSION = "1.4";
  NODE_SCOPE_HOOK = true;
}

PARSER_BEGIN(CSSParser)
package jaxx.css;

public class CSSParser {
    public SimpleNode popNode() {
        if ( jjtree.nodeArity() > 0)  // number of child nodes 
            return (SimpleNode)jjtree.popNode();
        else
            return null;
    }

    void jjtreeOpenNodeScope(Node n) {
        ((SimpleNode) n).firstToken = getToken(1);
    }

    void jjtreeCloseNodeScope(Node n) {
        ((SimpleNode) n).lastToken = getToken(0);
    }

  public static void main(String args[]) {
    System.out.println("Reading from standard input...");
    CSSParser css = new CSSParser(System.in);
    try {
      SimpleNode n = css.Stylesheet();
      n.dump("");
      System.out.println("Thank you.");
    } catch (Exception e) {
      System.out.println("Oops.");
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }
}

PARSER_END(CSSParser)


<DEFAULT, IN_RULE> SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
| <"//" (~["\n","\r"])* ("\n"|"\r"|"\r\n")>
| <"/*" (~["*"])* "*" (~["/"] (~["*"])* "*")* "/">
}

<*> TOKEN : /* LITERALS */
{
  <DECIMAL_LITERAL: <INTEGER_LITERAL> ("." <INTEGER_LITERAL>)?>
|
  <#INTEGER_LITERAL: (["0"-"9"])+>
}

<DEFAULT, IN_RULE> TOKEN : /* IDENTIFIER */
{
  <IDENTIFIER: <LETTER> (<LETTER>|<DIGIT>)*>
|
  <#LETTER: ["_", "-", "a"-"z", "A"-"Z"]>
|
  <#DIGIT: ["0"-"9"]>
}

<IN_PSEUDOCLASS> TOKEN : /* PSEUDOCLASS_IDENTIFIER */
{
  <PSEUDOCLASS_IDENTIFIER: <IDENTIFIER>> : DEFAULT
}

<DEFAULT> TOKEN: /* COLON */
{
  <PSEUDOCLASS_COLON: ":"> : IN_PSEUDOCLASS
}

<IN_RULE> TOKEN: /* COLON_IN_RULE */
{
  <COLON: ":"> 
}

<*> TOKEN: /* SEMICOLON */
{
  <SEMICOLON: ";">
}

TOKEN : /* LEFT BRACE */
{
  <LEFT_BRACE: "{"> : IN_RULE
}

<IN_RULE> TOKEN : /* RIGHT BRACE */
{
  <RIGHT_BRACE: "}"> : DEFAULT
}

<IN_RULE> TOKEN : /* JAVA_CODE_RULE START */
{
  <JAVA_CODE_START: <LEFT_BRACE>> : JAVA_CODE_RULE
}

<JAVA_CODE_RULE> TOKEN : /* JAVA_CODE_RULE */
{
  <JAVA_CODE: (~["}"])+ >
}

<JAVA_CODE_RULE> TOKEN : /* JAVA_CODE_RULE END */
{
  <JAVA_CODE_END: <RIGHT_BRACE>> : IN_RULE
}

  
<IN_PSEUDOCLASS> TOKEN : /* PROGRAMMATIC_PSEUDOCLASS */
{
  <PROGRAMMATIC_PSEUDOCLASS: "{" (~["}"])+ "}"> : DEFAULT
}
  
<IN_RULE> TOKEN : /* STRINGS */
{
  <STRING: "\"" (~["\"", "\\", "\n", "\r"])* "\"">
}

<IN_RULE> TOKEN : /* COLORS */
{
  <HEXCOLOR: "#" <HEXDIGIT> <HEXDIGIT> <HEXDIGIT> (<HEXDIGIT> <HEXDIGIT> <HEXDIGIT>)?>
|
  <#HEXDIGIT: ["0"-"9", "a"-"f", "A"-"F"]>
}


<IN_RULE> TOKEN : /* EMS */
{
  <EMS: <DECIMAL_LITERAL> "em">
}


<IN_RULE> TOKEN : /* EXS */
{
  <EXS: <DECIMAL_LITERAL> "ex">
}


<IN_RULE> TOKEN : /* LENGTH */
{
  <LENGTH: <DECIMAL_LITERAL> ("pt" | "mm" | "cm" | "pc" | "in")>
}


SimpleNode Stylesheet() : {}
{
  (Rule())*
  { return jjtThis; }
}


void Rule() : {}
{
  Selectors()
  <LEFT_BRACE> Declaration() (";" (Declaration())?)* <RIGHT_BRACE>
}


void Selectors() : {}
{
  Selector() ("," Selector())*
}


void Selector() : {}
{
  JavaClass() (Id())? (Class())? (PseudoClass())?
| 
  Id() (Class())? (PseudoClass())?
|
  Class() (PseudoClass())?
|
  PseudoClass()
}


void JavaClass() : {}
{
  <IDENTIFIER> | "*"
}


void Id() : {}
{
  "#" <IDENTIFIER>
}


void Class() : {}
{
  "." <IDENTIFIER>
}


void PseudoClass() : {}
{
  <PSEUDOCLASS_COLON> (<PSEUDOCLASS_IDENTIFIER> | <PROGRAMMATIC_PSEUDOCLASS>) (AnimationProperties())?
}


void AnimationProperties() : {}
{
  "[" AnimationProperty() ("," AnimationProperty())* "]"
}


void AnimationProperty() : {}
{
  <IDENTIFIER> "=" <DECIMAL_LITERAL> (<IDENTIFIER>)?
}


void Declaration() : {}
{
  Property() <COLON> Expression()
}


void Property() : {}
{
  <IDENTIFIER>
}


void Expression() : {}
{
   (<DECIMAL_LITERAL> | <STRING> | <IDENTIFIER> | <HEXCOLOR> | <EMS> | <EXS> | <LENGTH> |
      JavaCode())
}


void JavaCode() : {}
{
	<JAVA_CODE_START> <JAVA_CODE> <JAVA_CODE_END>
}


void Identifier() : {}
{
  <IDENTIFIER>
}
