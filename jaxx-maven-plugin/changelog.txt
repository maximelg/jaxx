1.3 chemit 20090409
  * 20090327 [chemit] - add javax help mecanism
  * 20090301 [chemit] - add a profile mode (-Djaxx.profile)

1.2 letelier 2009022?

1.1 chemit 20090220
  * 20090203 [chemit] - mojo's property src is now by default in src/main/java
  * 20090202 [chemit] - introduce a property validatorClass to specify the validator implementation
  * 20090124 [chemit] - introduce a flag useUIManagerForIcon to retreave icons from UIManager
                      - clean mojo getter and setter (not used here)
  * 20090123 [chemit] - add tests for icon improvment
  * 20090122 [chemit] - refactor poms (sibling dependencies, pluginsManagment,...)
                      - rename i18n bundles according artifactId
  * 20090113 [chemit] - fix bug : when using addProjectClassPath property, sometimes does not retreave
                       dependancies, force it to work whatever is the phase :)

1.0 chemit 20090111

0.8 chemit 200812??
  * 20081219 [chemit] - add addProjectClassPath property to add project compile class-path in plugin class-path
  * 20081214 [chemit] - add addSourcesToClassPath property to add sources directories in class-path

0.7 chemit 20081210
  * 20081208 [chemit] - javabBean attribute use to initialize bean
  * 20081207 [chemit] use lutinproject 3.1
 
0.6 chemit 200811??
  * 20081108 [chemit] can add extra imports in JaxxGeneratorMojo (will be added to all generated java files).
  * 20081104 [chemit] can add extra beanInfoSearchPath in JaxxGeneratorMojo

0.5 chemit 20081002
  * 20081013 [chemit] can generate logger on jaxx files
  * 20081011 [chemit] improve site
  * 20081011 [chemit] refactor tests of the plugin using maven-plugin-testing-harness plugin
  * 20081002 [chemit] Using lutinpluginproject 3.0, changing groupId to org.codelutin
  * 20081002 [chemit] Make nearly all tests works again...
