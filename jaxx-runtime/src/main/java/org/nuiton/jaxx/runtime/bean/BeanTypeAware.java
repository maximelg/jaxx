package org.nuiton.jaxx.runtime.bean;

/*
 * #%L
 * JAXX :: Runtime
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Contract for ui which contains a {@code beanType} property.
 *
 * In a jaxx file, if an object has a genericType and implementsthis contract
 * then the {@code beanType} will be automaticly setted.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.9
 */
public interface BeanTypeAware<O> {

    String PROPERTY_BEAN_TYPE = "beanType";

    Class<O> getBeanType();

    void setBeanType(Class<O> beanType);
}
