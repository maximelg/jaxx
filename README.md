# JAXX

[![Maven Central status](https://img.shields.io/maven-central/v/org.nuiton/jaxx.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22org.nuiton%22%20AND%20a%3A%22jaxx%22)
![Build Status](https://gitlab.nuiton.org/nuiton/jaxx/badges/develop/build.svg)

# Resources

* [Changelog](https://gitlab.nuiton.org/nuiton/jaxx/blob/develop/CHANGELOG.md)
* [Documentation](http://jaxx.nuiton.org)

# Community

* [Mailing-list (Users)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-users)
* [Mailing-list (Devel)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-devel)
* [Mailing-list (Commits)](http://list.forge.nuiton.org/cgi-bin/mailman/listinfo/jaxx-commits)
