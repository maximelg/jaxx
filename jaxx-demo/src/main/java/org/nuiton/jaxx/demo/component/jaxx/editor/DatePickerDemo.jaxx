<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    java.beans.PropertyChangeEvent
    java.beans.PropertyChangeListener
    java.text.SimpleDateFormat
    java.util.Date
    org.nuiton.jaxx.widgets.datetime.JAXXDatePicker
  </import>

  <String id='patternLayout' javaBean='"dd/MM/yyyy HH:mm:ss"'/>

  <SimpleDateFormat id='dateFormat' constructorParams='patternLayout'/>

  <Date id='date' javaBean='new Date()'/>

  <script><![CDATA[
    protected void $afterCompleteSetup() {

        addPropertyChangeListener(PROPERTY_PATTERN_LAYOUT, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                dateFormat = new SimpleDateFormat(patternLayout);
                datePicker.setPatternLayout(patternLayout);
                resultView.setText(dateFormat.format(date));
            }
        });

        addPropertyChangeListener(PROPERTY_DATE, new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                resultView.setText(dateFormat.format(date));
            }
        });
    }
  ]]></script>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell columns='2' fill='horizontal' weightx='1'>
        <JCheckBox id='showPopupButton'
                   text='jaxxdemo.datePickerEditor.showPopupButton'
                   selected='false'/>
      </cell>
    </row>
    <row>
      <cell>
        <JLabel text='jaxxdemo.datePickerEditor.patternLayout'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JTextField id='patternLayoutField'
                    text='{patternLayout}'
                    onFocusLost='setPatternLayout(patternLayoutField.getText())'/>
      </cell>
    </row>
    <row>
      <cell fill='horizontal' weightx='1' columns='2'>
        <JAXXDatePicker id='datePicker'
                        date='{date}'
                        patternLayout='{patternLayout}'
                        showPopupButton='{showPopupButton.isSelected()}'
                        onActionPerformed='setDate(datePicker.getDate())'/>
      </cell>
    </row>
    <row>
      <cell>
        <JLabel text='jaxxdemo.datePickerEditor.dateResult'/>
      </cell>
      <cell fill='horizontal' weightx='1'>
        <JLabel id='resultView' text='{dateFormat.format(date)}'/>
      </cell>
    </row>
  </Table>
</org.nuiton.jaxx.demo.DemoPanel>
