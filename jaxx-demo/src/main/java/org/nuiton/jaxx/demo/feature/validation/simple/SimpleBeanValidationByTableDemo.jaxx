<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>
  <style source="Validation.jcss"/>

  <import>
    javax.swing.JOptionPane
    static org.nuiton.i18n.I18n.n
    org.nuiton.jaxx.demo.entities.Identity
    org.nuiton.jaxx.demo.entities.Model
    org.nuiton.jaxx.validator.swing.SwingValidatorUtil
    org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableRenderer
    org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel

    java.io.File
  </import>

  <script><![CDATA[
void $afterCompleteSetup() {
    SwingValidatorUtil.installUI(errorTable, new SwingValidatorMessageTableRenderer());
}

@Override
protected String[] getSources() {
    return addDefaultSources (
        "Validation.css",
        "/org.nuiton.jaxx.demo.entities.Identity.java",
        "/org.nuiton.jaxx.demo.entities.Identity-error-validation.xml",
        "/org.nuiton.jaxx.demo.entities.Identity-info-validation.xml",
        "/org.nuiton.jaxx.demo.entities.Identity-warning-validation.xml",
        "/org.nuiton.jaxx.demo.entities.Model.java",
        "/org.nuiton.jaxx.demo.entities.Model-error-validation.xml",
        "/org.nuiton.jaxx.demo.entities.Model-info-validation.xml",
        "/org.nuiton.jaxx.demo.entities.Model-warning-validation.xml"
    );
}
]]></script>

  <!-- models -->
  <Model id='model1'/>
  <Model id='model2'/>
  <Identity id='identity'/>

  <!-- errors model -->
  <SwingValidatorMessageTableModel id='errorTableModel'
                                   onTableChanged='ok.setEnabled(errorTableModel.getRowCount()==0)'/>

  <!-- validators -->
  <BeanValidator id='validator' bean='model1'
                 uiClass="org.nuiton.jaxx.validator.swing.ui.ImageValidationUI">
    <field name="text"/>
    <field name="text2"/>
    <field name="ratio"/>
  </BeanValidator>
  <BeanValidator id='validator2' bean='model2'
                 uiClass="org.nuiton.jaxx.validator.swing.ui.IconValidationUI">
    <field name="text" component="_text"/>
    <field name="text2" component="_text2"/>
    <field name="ratio" component="_ratio"/>
  </BeanValidator>
  <BeanValidator id='validator3' autoField='true' bean='identity'
                 uiClass="org.nuiton.jaxx.validator.swing.ui.TranslucentValidationUI">
    <field name="email" component="email2"/>
  </BeanValidator>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Form")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='text' text='{model1.getText()}'
                            onKeyReleased='model1.setText(text.getText())'
                            _validatorLabel='{n("form.text")}'
                />
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='text2' text='{model1.getText2()}'
                            onKeyReleased='model1.setText2(text2.getText())'
                            _validatorLabel='{n("form.text2")}'
                />
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JSlider id='ratio' minimum='0' maximum='100'
                         value='{model1.getRatio()}'
                         _validatorLabel='{n("form.ratio")}'
                         onStateChanged='model1.setRatio(ratio.getValue())'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Model")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model1.getText()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model1.getText2()}'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JLabel text='{model1.getRatio()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Form2")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='_text' text='{model2.getText()}'
                            _validatorLabel='{n("form2.text")}'
                            onKeyReleased='model2.setText(_text.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='_text2' text='{model2.getText2()}'
                            _validatorLabel='{n("form2.text2")}'
                            onKeyReleased='model2.setText2(_text2.getText())'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JSlider id='_ratio' minimum='0' maximum='100'
                         value='{model2.getRatio()}'
                         _validatorLabel='{n("form2.ratio")}'
                         onStateChanged='model2.setRatio(_ratio.getValue())'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Model2")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='Text:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model2.getText()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Text2:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{model2.getText2()}'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Ratio:'/>
              </cell>
              <cell>
                <JLabel text='{model2.getRatio()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Identify Form")}'
                layout='{new GridLayout()}' width='250' height='180'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='FirstName:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='firstName' text='{identity.getFirstName()}'
                            onKeyReleased='identity.setFirstName(firstName.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='LastName:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='lastName' text='{identity.getLastName()}'
                            onKeyReleased='identity.setLastName(lastName.getText())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Email:'/>
              </cell>
              <cell weightx='1'>
                <JTextField id='email2' text='{identity.getEmail()}'
                            onKeyReleased='identity.setEmail(email2.getText())'/>
              </cell>
            </row>

            <row>
              <cell>
                <JLabel text='Age:'/>
              </cell>
              <cell>
                <JSlider id='age' minimum='0' maximum='100'
                         value='{identity.getAge()}'
                         onStateChanged='identity.setAge(age.getValue())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Config file :'/>
              </cell>
              <cell>
                <JTextField id='config' text='{identity.getConfig()+""}'
                            onKeyReleased='identity.setConfig(new File(config.getText()))'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Working directory:'/>
              </cell>
              <cell>
                <JTextField id='dir' text='{identity.getDir()+""}'
                            onKeyReleased='identity.setDir(new File(dir.getText()))'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Identity Model")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <Table anchor='west' fill='both'>
            <row>
              <cell>
                <JLabel text='FirstName:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getFirstName()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='LastName:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getLastName()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Email:'/>
              </cell>
              <cell weightx='1'>
                <JLabel text='{identity.getEmail()}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Age:'/>
              </cell>
              <cell>
                <JLabel text='{identity.getAge()+""}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Config file:'/>
              </cell>
              <cell>
                <JLabel text='{identity.getConfig()+""}'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel text='Directory file:'/>
              </cell>
              <cell>
                <JLabel text='{identity.getDir()+""}'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell columns='2' fill="both">
        <JPanel border='{BorderFactory.createTitledBorder("Messages")}'
                layout='{new GridLayout()}' height='200'
                width='500'>
          <JScrollPane columnHeaderView='{errorTable.getTableHeader()}'>
            <JTable id='errorTable' model='{errorTableModel}' rowSelectionAllowed='true'
                    autoCreateRowSorter='true'
                    autoResizeMode='2' cellSelectionEnabled='false'
                    selectionMode='0'/>
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell columns='2' fill="both">
        <JPanel layout='{new GridLayout(1,2,0,0)}'>
          <JButton id='cancel' text='cancel'
                   onActionPerformed='JOptionPane.showMessageDialog(this, cancel.getText() + " clicked!", "onActionPerformed", JOptionPane.INFORMATION_MESSAGE);'/>
          <JButton id='ok' text='valid'
                   onActionPerformed='JOptionPane.showMessageDialog(this, ok.getText() + " clicked!", "onActionPerformed", JOptionPane.INFORMATION_MESSAGE);'/>
        </JPanel>
      </cell>
    </row>
  </Table>

</org.nuiton.jaxx.demo.DemoPanel>
