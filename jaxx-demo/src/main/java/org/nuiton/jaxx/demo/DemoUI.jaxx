<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<JFrame id='mainFrame' width='1024' height='800'
        decorator='help' onWindowClosing='handler.close(mainFrame)'>

  <import>
    org.nuiton.jaxx.demo.tree.DemoTreeHelper
    org.nuiton.jaxx.demo.tree.DemoCellRenderer

    org.nuiton.jaxx.runtime.swing.SwingUtil
    org.nuiton.jaxx.runtime.swing.CardLayout2
    org.nuiton.jaxx.widgets.status.StatusMessagePanel

    java.awt.Dimension
    java.util.Locale
  </import>

  <CardLayout2 id='contentLayout'/>

  <DemoConfig id='config' initializer='getContextValue(DemoConfig.class)'/>

  <DemoTreeHelper id='treeHelper'
                  initializer='getContextValue(DemoTreeHelper.class)'/>

  <DemoHelpBroker id='broker' constructorParams='"ui.main.menu"'/>

  <script><![CDATA[

public boolean acceptLocale(Locale l, String expected) {
  return l !=null && l.toString().equals(expected);
}
]]>
  </script>

  <JMenuBar id='menu'>

    <JMenu id='menuFile'>
      <JMenuItem id='menuFileConfiguration'
                 onActionPerformed="handler.showConfig(this)"/>
      <JMenu id='menuFileLanguage'>
        <JMenuItem id='menuFileLanguageFR'
                   onActionPerformed="handler.changeLanguage(this, Locale.FRANCE)"/>
        <JMenuItem id='menuFileLanguageUK'
                   onActionPerformed="handler.changeLanguage(this, Locale.UK)"/>
      </JMenu>
      <JSeparator/>
      <JMenuItem id='menuFileFullscreen'
                 onActionPerformed="handler.changeScreen(this, true)"/>
      <JMenuItem id='menuFileNormalscreen'
                 onActionPerformed="handler.changeScreen(this, false)"/>
      <JSeparator/>
      <JMenuItem id='menuFileExit'
                 onActionPerformed="handler.close(this)"/>
    </JMenu>

    <JMenu id='menuHelp'>
      <JMenuItem id='menuHelpHelp'
                 onActionPerformed="handler.showHelp(this, null)"/>
      <JMenuItem id='menuHelpSite'
                 onActionPerformed="handler.gotoSite(this)"/>
      <JMenuItem id='menuHelpAbout'
                 onActionPerformed="handler.showAbout(this)"/>
    </JMenu>

    <JToolBar layout='{new BorderLayout()}'>

      <!-- pour afficher l'aide contextuelle -->
      <JButton id='showHelp' constraints='BorderLayout.EAST'/>
    </JToolBar>


  </JMenuBar>

  <JPanel id='mainPane' layout='{new BorderLayout()}'>

    <JSplitPane id='splitPane' constraints='BorderLayout.CENTER'>
      <JScrollPane id='navigationPane'>
        <JTree id='navigation'/>
      </JScrollPane>

      <JPanel id='content'/>

    </JSplitPane>

    <StatusMessagePanel id='p' constraints='BorderLayout.SOUTH'/>

  </JPanel>
</JFrame>
