<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    java.awt.Color
    javax.swing.BorderFactory
    javax.swing.border.BevelBorder
  </import>

  <!--<style source='CalculatorDemo.jcss'/>-->
  <script><![CDATA[
    plus.setText("+");
    sign.setText("+/-");

@Override
protected String[] getSources() {
    return addDefaultSources( "CalculatorEngine.java" );
}
]]>
  </script>
  <!-- use fully-qualified name just in case this is compiled into a different package -->
  <CalculatorEngine id='engine'/>

  <Table fill='both'>
    <row>
      <cell columns='4'>
        <JLabel id='display' text='{engine.getDisplayText()}'/>
      </cell>
    </row>

    <row>
      <cell columns='2'>
        <JButton id='c' text='C' onActionPerformed='engine.clear()'
                 styleClass='clear'/>
      </cell>
      <cell>
        <JButton id='ce' text='CE' onActionPerformed='engine.clearEntry()'
                 styleClass='clear'/>
      </cell>
      <cell>
        <JButton id='equals' text='=' onActionPerformed='engine.equal()'
                 styleClass='operator'/>
      </cell>
    </row>

    <row>
      <cell>
        <JButton id='d7' text='7' onActionPerformed='engine.digit(7)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d8' text='8' onActionPerformed='engine.digit(8)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d9' text='9' onActionPerformed='engine.digit(9)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='plus' onActionPerformed='engine.add()'
                 styleClass='operator'/>
      </cell>
    </row>

    <row>
      <cell>
        <JButton id='d4' text='4' onActionPerformed='engine.digit(4)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d5' text='5' onActionPerformed='engine.digit(5)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d6' text='6' onActionPerformed='engine.digit(6)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='subtract' text='-' onActionPerformed='engine.subtract()'
                 styleClass='operator'/>
      </cell>
    </row>

    <row>
      <cell>
        <JButton id='d1' text='1' onActionPerformed='engine.digit(1)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d2' text='2' onActionPerformed='engine.digit(2)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='d3' text='3' onActionPerformed='engine.digit(3)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='multiply' text='x' onActionPerformed='engine.multiply()'
                 styleClass='operator'/>
      </cell>
    </row>

    <row>
      <cell>
        <JButton id='d0' text='0' onActionPerformed='engine.digit(0)'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='sign' onActionPerformed='engine.toggleSign()'
                 styleClass='operator'/>
      </cell>
      <cell>
        <JButton id='dot' text='.' onActionPerformed='engine.dot()'
                 styleClass='digit'/>
      </cell>
      <cell>
        <JButton id='divide' text='&#x00F7;' onActionPerformed='engine.divide()'
                 styleClass='operator'/>
      </cell>
    </row>
  </Table>
</org.nuiton.jaxx.demo.DemoPanel>
