<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    org.jdesktop.swingx.JXTable
    javax.swing.JOptionPane
    static org.nuiton.i18n.I18n.n
    org.nuiton.jaxx.demo.entities.People
    org.nuiton.validator.bean.list.BeanListValidator
    org.nuiton.jaxx.validator.swing.SwingListValidatorMessageTableModel

    java.io.File
  </import>

  <script><![CDATA[
@Override
protected String[] getSources() {
    return addDefaultSources(
        "ListBeanValidationDemo.css",
        "/org.nuiton.jaxx.demo.entities.People.java",
        "/org.nuiton.jaxx.demo.entities.People-error-validation.xml",
        "/org.nuiton.jaxx.demo.entities.People-info-validation.xml",
        "/org.nuiton.jaxx.demo.entities.People-warning-validation.xml",
        "ListBeanValidationDemoHandler.java",
        "PeopleTableModel.java"
    );
}
]]></script>

  <!-- model -->
  <PeopleTableModel id='model'/>

  <!-- validator -->
  <BeanListValidator id='validator' genericType='People'
                     initializer='BeanListValidator.newValidator(People.class, null)'/>

  <!-- errors model -->
  <SwingListValidatorMessageTableModel id='errorTableModel'
                                       onTableChanged='handler.updateOkEnabled()'/>

  <Table fill='both' constraints='BorderLayout.CENTER'>
    <row>
      <cell weightx='1' weighty='1' insets='6, 3, 0, 0'>
        <JPanel border='{BorderFactory.createTitledBorder("Form")}'
                layout='{new GridLayout()}' width='250' height='120'>
          <JScrollPane id='dataTableScrollePane' constraints='BorderLayout.CENTER'>
            <JXTable id='dataTable' model='{model}' autoCreateRowSorter='true'/>
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell fill="both">
        <JPanel layout='{new GridLayout(1,3,0,0)}'>
          <JButton id='addIdentity' text='jaxx.demo.action.add'
                   onActionPerformed='handler.addPeople()'/>
          <JButton id='removeIdentity' text='jaxx.demo.action.remove'
                   onActionPerformed='handler.removePeople()'/>
          <JButton id='ok' text='jaxx.demo.action.ok'
                   onActionPerformed='JOptionPane.showMessageDialog(this, ok.getText() + " clicked!", "onActionPerformed", JOptionPane.INFORMATION_MESSAGE);'/>

        </JPanel>
      </cell>
    </row>
    <row>
      <cell fill="both">
        <JPanel border='{BorderFactory.createTitledBorder("Messages")}'
                layout='{new GridLayout()}' height='200'
                width='500'>
          <JScrollPane columnHeaderView='{errorTable.getTableHeader()}'>
            <JTable id='errorTable' model='{errorTableModel}'
                    rowSelectionAllowed='true'
                    autoCreateRowSorter='true'
                    autoResizeMode='2' cellSelectionEnabled='false'
                    selectionMode='0'/>
          </JScrollPane>
        </JPanel>
      </cell>
    </row>
  </Table>

</org.nuiton.jaxx.demo.DemoPanel>
